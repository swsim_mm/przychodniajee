# README #

------------------------------
> Elementy przygotowane proszę oznaczać
> + dla elementu 100% wykonanego - jeśli nie jest gotowy
> za znakiem można umieszczać % ukończenia (- 20% nazwa)
------------------------------

# Do zrobienia/zrobione #

------------------------------
### Ogólne ###
------------------------------

+ Temat i motyw strony
	+ (zmniejszyło się zapotrzebowanie gdyż wykorzystano bootstrap)


+ Logowanie i sesja
	+ logowanie z użytkownikiem
	+ identyfikacja sesji
	+ podbijanie sesji
	+ identyfikacja uzytkownika (blokwanie stron)
	
	
+ Strona główna (kafle albo panele)
	+ dla niezalogowanych
	+ dla zalogowanych


- 75% O pojekcie
	+ Informacje ogólne
	+ Dane do startowego logowania
	+ technologie i biblioteki
	- Mapa dokumentacji
	
------------------------------
### Wyciaganie danych z bazy ###
------------------------------
	
	
- Wizyty
	- lista
	- formularz
	- szablon


- Zabiegi 
	- lista
	- formularz
	- szablon


- Rozpoznania
	- lista
	- formularz
	- szablon
	
------------------------------

+ Pacjenci
	+ lista
	+ formularz
	+ szablon


+ Pracownicy 
	+ lista
	+ formularz
	+ szablon


- 75% Użytkownicy
	+ lista
	+ formularz
	+ szablon
	- ACL
	
------------------------------	
	
+ Gabinety
	+ lista
	+ formularz
	+ szablon
	
	
+ Choroby
	+ lista
	+ formularz
	+ szablon


+ Rodzaje wizyt
	+ lista
	+ formularz
	+ szablon


- 90% Rodzaje zabiegów
	+ lista
	+ formularz
	- szablon

	
------------------------------	

- Prosty instalator i konfigurator
	