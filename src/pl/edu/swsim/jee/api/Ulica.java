package pl.edu.swsim.jee.api;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.zw.dao.LokalizacjeRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;

/**
 * Servlet implementation class Miasto
 */
@WebServlet(description = "Lista ulic bezpośrednio przez jquery i ajax", urlPatterns = {
		"/api/Ulica", "/api/ulica" })
public class Ulica extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() {}.getClass().getEnclosingClass());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Ulica() {
		super();
		logger.trace(".");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);

		if (!app.isLogedIn()) {
			app.OutPrint("logout :/");
			return;
		}
		if (request.getParameter("id") == null) {
			app.OutPrint("no id :/");
			return;
		}

		Long id = 0L;
		try {
			id = Long.parseLong(request.getParameter("id"));
		} catch (Exception e) {
			app.OutPrint("NaN");
			return;
		}
		
		response.setContentType("application/json;charset=UTF-8");

		LokalizacjeRep lokalizacje = new LokalizacjeRep();
		lokalizacje.setMiasto(new MiastoEnt(id));
		
		if(request.getParameter("q") != null) 
			lokalizacje.setSearchText(URLDecoder.decode(request.getParameter("q"), "UTF-8"));
			
		ArrayList<UlicaEnt> ulice = lokalizacje.getUlice();
		

		String result = "[" + System.lineSeparator();
		//String result = "{";

		for (int i = 0; i < ulice.size(); i++) {
			result += "{ \"value\": \"";
			result += Long.toString(ulice.get(i).getId());
			result += "\", \"label\": \"";
			result += ulice.get(i).getNazwa();
			result += "\"}";
			if(i != ulice.size()-1)
				result += ",";
			result += System.lineSeparator();
		}

		 //result += "]}";
		result += "]";


		app.OutPrint(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
