package pl.edu.swsim.jee.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.PracownikEnt;

/**
 * Servlet implementation class WolneTerminy
 */
@WebServlet(
		description = "wolne terminy w wyznaczonym dniu", 
		urlPatterns = { 
				"/api/wolneterminy", "/api/WolneTerminy"
		})
public class WolneTerminy extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() {}.getClass().getEnclosingClass());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WolneTerminy() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);

		if (!app.isLogedIn()) {
			app.OutPrint("logout :/");
			return;
		}
		
		String dateText = request.getParameter("date");
		String lekarzId = request.getParameter("lekarz-id");
		
		
		if (dateText == null || lekarzId == null) {
			app.OutPrint("brak daty lub lekarza");
			return;
		}
				
		TerminyRep repository = new TerminyRep();
		
		
        String[] dateData = dateText.split("\\.");
        
    	if(dateData.length != 3) {
    		dateText = todayDate();
    		dateData = dateText.trim().split("\\.");
    	}
    	
    	logger.trace(Integer.parseInt(dateData[0]) + " " + Integer.parseInt(dateData[1]) + " " + Integer.parseInt(dateData[2]));
    	
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.YEAR, Integer.parseInt(dateData[0]));
    	cal.set(Calendar.MONTH, Integer.parseInt(dateData[1])-1);
    	cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateData[2]));
				
		repository.setThatDate(cal.getTime());
		
		Long id = 0L;
		try {
			id = Long.parseLong(lekarzId);
		} catch (Exception e) {
			app.OutPrint("NaN");
			return;
		}
		
		ArrayList<PracownikEnt> prac = new PracownicyRep().setId(id).getPracownicy();
		if(prac.size() != 1) {
			app.OutPrint("Zły pracownik");
			return;
		}
		
		repository.setLekarz(prac.get(0));
		
		/*// mimo że przekazywany jest cały wpis typu to wyszukiwanie skupia się na gabientach
		if(this.rdbtnWizyta.isSelected())
			repository.setRodzajWizyty(this.rodzajeWizyt.get(cbRodzajeTerminu.getSelectedIndex()));
		else
			repository.setRodzajZabiegu(this.rodzajeZabiegow.get(cbRodzajeTerminu.getSelectedIndex()));
		
		// sortowanie rosnąco
		ArrayList<Order> orderList = new ArrayList<Order>();
		orderList.add(Order.asc("termin"));
		repository.setOrderBy(orderList);
		
		ArrayList<TerminEnt> terminy = repository.getTerminy();
		
		// w tym momencie posiadamy listę terminów zarejestrowanych na dany dzień
		// dla danego gabinetu, potrzeba jest odwrotna lista wolnych terminów
		
		
		java.sql.Time czasTrwaniaWybranej; // Czas trwania wybranej wizyty w aktualnym oknie

		// zdobycie czastu trwania wybranego terminu
		if(this.rdbtnWizyta.isSelected())
			czasTrwaniaWybranej = ((RodzajWizytyEnt)this.rodzajeWizyt
					.get(cbRodzajeTerminu.getSelectedIndex())).getCzastrwania();
		else
			czasTrwaniaWybranej = ((RodzajZabieguEnt)this.rodzajeZabiegow
					.get(cbRodzajeTerminu.getSelectedIndex())).getCzastrwania();
		
		// liczniki czasu 
		Calendar calRecord = Calendar.getInstance(); // operacje na danych rekordu
		Calendar calCounter = Calendar.getInstance();  // licznik przeglądania czasu w dniu
		Calendar calDiff = Calendar.getInstance();  // obliczanie róznicy
		
		// to nie jest koniecznie potrzebne
		calCounter.set(Calendar.YEAR, this.panelData.getModel().getYear());
		calCounter.set(Calendar.MONTH, this.panelData.getModel().getMonth());
		calCounter.set(Calendar.DAY_OF_MONTH, this.panelData.getModel().getDay());
		
		// odczytanie długości wybranego typu terminu (który stara się zmieścić)
		calRecord.setTime(czasTrwaniaWybranej);
		int wybranaGodzina = calRecord.get(Calendar.HOUR_OF_DAY);
		int wybranaMinuta = calRecord.get(Calendar.MINUTE);
		
		// to już tak, reset od godziny 0:00, od początku dnia
		calCounter.set(Calendar.HOUR_OF_DAY, 0);
		calCounter.set(Calendar.MINUTE, 0);
		calCounter.set(Calendar.SECOND, 0); 
		
		for(int i=0; i<terminy.size(); i++) {
			// Różnica między poprzednim rekordem a bieżącym
			calDiff.setTime( new Date(terminy.get(i).getTermin().getTime() - calCounter.getTimeInMillis()) );
			
			if( (calDiff.get(Calendar.HOUR_OF_DAY) > wybranaGodzina || calDiff.get(Calendar.MINUTE) > wybranaMinuta )
					&& terminy.get(i).getTermin().getTime() > calCounter.getTimeInMillis()
					) {
				// jeśli starczy czasu na wizytę
				WolneTerminy wt = new WolneTerminy();
				wt.terminOd = new Date(calCounter.getTime().getTime()); //od 
				wt.terminDo = new Date(terminy.get(i).getTermin().getTime()); // do	
				
				calRecord.setTime(calCounter.getTime());
				wt.text = Integer.toString(calRecord.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(calRecord.get(Calendar.MINUTE));
				wt.text += " - ";
				calRecord.setTime(terminy.get(i).getTermin());
				wt.text += Integer.toString(calRecord.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(calRecord.get(Calendar.MINUTE));
				
				wolneTerminy.add(wt);
			}
			
			if(rdbtnZabieg.isSelected()) 
				calRecord.setTime(terminy.get(i).getRodzajzabiegu().getCzastrwania());
			else 
				calRecord.setTime(terminy.get(i).getRodzajwizyty().getCzastrwania());
			
			calCounter.setTime(terminy.get(i).getTermin());
			calCounter.add(Calendar.HOUR_OF_DAY, calRecord.get(Calendar.HOUR_OF_DAY));
			calCounter.add(Calendar.MINUTE, calRecord.get(Calendar.MINUTE));

		}
					
		//ostatni wspis do końca czasu
		WolneTerminy wt = new WolneTerminy();
		wt.terminOd = new Date(calCounter.getTime().getTime()); //od 
		
		calDiff.setTime(new Date(calCounter.getTime().getTime()));
		calDiff.set(Calendar.HOUR_OF_DAY, 23);
		calDiff.set(Calendar.MINUTE, 59);
		calDiff.set(Calendar.SECOND, 59);
		
		wt.terminDo = new Date(calDiff.getTimeInMillis()); // do	
		
		calRecord.setTime(calCounter.getTime());
		wt.text = Integer.toString(calRecord.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(calRecord.get(Calendar.MINUTE));
		wt.text += " - +++";

		wolneTerminy.add(wt);
		
		
		
		DefaultListModel<String> model = new DefaultListModel<String>();
		
		for(int i=0; i<wolneTerminy.size(); i++) {
			model.addElement(wolneTerminy.get(i).text);
		}
		
		listWolneTerminy.setModel(model);
		
		
		
		//--------------------------------------------------------------------------------------------------------------------
		
		boolean isZabieg = false;
		
		if (request.getParameter("zabieg") != null && request.getParameter("zabieg").compareToIgnoreCase("tak")==0) {
			isZabieg = true;
		}
		
		response.setContentType("application/json;charset=UTF-8");

		String result = "[" + System.lineSeparator();

		
		if(isZabieg) {
			RodzajeZabiegowRep repo = new RodzajeZabiegowRep();
			
			if(request.getParameter("q") != null) 
				repo.setSearchText(URLDecoder.decode(request.getParameter("q"), "UTF-8"));
				
			ArrayList<RodzajZabieguEnt> items = repo.getRodzajeZabiegow();
			

			for (int i = 0; i < items.size(); i++) {
				result += "{ \"value\": \"";
				result += Long.toString(items.get(i).getId());
				result += "\", \"label\": \"";
				result += items.get(i).getNazwa() + " (" + items.get(i).getGabinet().getNumer() 
						+ " " + items.get(i).getGabinet().getNazwa() + ")";
				result += "\"}";
				if(i != items.size()-1)
					result += ",";
				result += System.lineSeparator();
			}
		} else {
			RodzajeWizytRep repo = new RodzajeWizytRep();
			
			if(request.getParameter("q") != null) 
				repo.setSearchText(URLDecoder.decode(request.getParameter("q"), "UTF-8"));
				
			ArrayList<RodzajWizytyEnt> items = repo.getRodzajeWizyt();
			

			for (int i = 0; i < items.size(); i++) {
				result += "{ \"value\": \"";
				result += Long.toString(items.get(i).getId());
				result += "\", \"label\": \"";
				result += items.get(i).getNazwa() + " (" + items.get(i).getGabinet().getNumer() 
						+ " " + items.get(i).getGabinet().getNazwa() + ")";
				result += "\"}";
				if(i != items.size()-1)
					result += ",";
				result += System.lineSeparator();
			}
		}
		

		 //result += "]}";
		result += "]";


		app.OutPrint(result);*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private String todayDate() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");	        		
        return format1.format(cal.getTime()); 
	}

}
