package pl.edu.swsim.jee.api;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.entities.PacjentEnt;

/**
 * Servlet implementation class Pacjent
 */
@WebServlet(
		description = "PacjentApi", 
		urlPatterns = { 
				"/api/pacjent", 
				"/api/Pacjent"
		})
public class Pacjent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() {}.getClass().getEnclosingClass());

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pacjent() {
        super();
		logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);

		if (!app.isLogedIn()) {
			app.OutPrint("logout :/");
			return;
		}
		if (request.getParameter("q") == null) {
			app.OutPrint("no text to search");
			return;
		}
		
		response.setContentType("application/json;charset=UTF-8");

		PacjenciRep repo = new PacjenciRep();		
		repo.setSearchText(URLDecoder.decode(request.getParameter("q"), "UTF-8"));
		
			
		ArrayList<PacjentEnt> items = repo.getPacjenci();
		

		String result = "[" + System.lineSeparator();


		for (int i = 0; i < items.size(); i++) {
			result += "{ \"value\": \"";
			result += Long.toString(items.get(i).getId());
			result += "\", \"label\": \"";
			result += items.get(i).getImie() + " " + items.get(i).getNazwisko();
			result += " - " + items.get(i).getUlica().getNazwa() + " " + items.get(i).getBudynek() + " " 
					+ items.get(i).getMieszkanie() + ", " + items.get(i).getUlica().getMiasto().getNazwa() 
					+ " " + items.get(i).getUlica().getMiasto().getKod_pocztowy() + ", " 
					+ items.get(i).getUlica().getMiasto().getWojewodztwo().getNazwa(); 
			result += "\"}";
			if(i != items.size()-1)
				result += ",";
			result += System.lineSeparator();
		}

		result += "]";

		app.OutPrint(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
