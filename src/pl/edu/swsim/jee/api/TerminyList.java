package pl.edu.swsim.jee.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Servlet implementation class TerminyList
 */
@WebServlet(
		description = "lista wizyt i zabiegów pacjenta", 
		urlPatterns = { 
				"/api/terminylist", 
				"/api/TerminyList"
		})
public class TerminyList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() {}.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TerminyList() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);

		if (!app.isLogedIn()) {
			app.OutPrint("logout :/");
			return;
		}
		
		String pacjent = request.getParameter("pacjent");
		
		
		if (pacjent == null) {
			app.OutPrint("brak id pacjenta");
			return;
		}
		
		Long id = 0L;
		try {
			id = Long.parseLong(pacjent);
		} catch (Exception e) {
			app.OutPrint("NaN");
			return;
		}
				
		TerminyRep repository = new TerminyRep();
		
		ArrayList<PacjentEnt> pacjenci = new PacjenciRep().setId(id).getPacjenci();
		if(pacjenci.size() != 1) {
			app.OutPrint("Zły id");
		} 
		
		repository.setPacjent(pacjenci.get(0));
		
		ArrayList<Order> orderList = new ArrayList<Order>();
		orderList.add(Order.asc("termin"));
		repository.setOrderBy(orderList);
		
		ArrayList<TerminEnt> terminy = repository.getTerminy();
		
		//---------------------------------------------------
		
		response.setContentType("application/json;charset=UTF-8");

		String result = "[" + System.lineSeparator();
		
		for (int i = 0; i < terminy.size(); i++) {
			result += "{ \"value\": \"";
			result += Long.toString(terminy.get(i).getId());
			result += "\", \"label\": \"";
			
    		SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd HH:mm");	        		
			String termin = format1.format(terminy.get(i).getTermin());
			
			result += termin;
			result += (terminy.get(i).isZabieg()?" Zabieg":" Wizyta");
			result += "\"}";
			if(i != terminy.size()-1)
				result += ",";
			result += System.lineSeparator();
		}

		result += "]";

		app.OutPrint(result);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
