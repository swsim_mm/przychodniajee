package pl.edu.swsim.jee.api;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.zw.dao.RodzajeWizytRep;
import pl.edu.swsim.zw.dao.RodzajeZabiegowRep;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;

/**
 * Servlet implementation class Rodzaj
 */
@WebServlet(
		description = "Rodzaj zabiegu lub wizyty", 
		urlPatterns = { 
				"/api/rodzaj", 
				"/api/Rodzaj"
		})
public class Rodzaj extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() {}.getClass().getEnclosingClass());

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Rodzaj() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);

		if (!app.isLogedIn()) {
			app.OutPrint("logout :/");
			return;
		}
		
		boolean isZabieg = false;
		
		if (request.getParameter("zabieg") != null && request.getParameter("zabieg").compareToIgnoreCase("tak")==0) {
			isZabieg = true;
		}
		
		response.setContentType("application/json;charset=UTF-8");

		String result = "[" + System.lineSeparator();

		
		if(isZabieg) {
			RodzajeZabiegowRep repo = new RodzajeZabiegowRep();
			
			if(request.getParameter("q") != null) 
				repo.setSearchText(URLDecoder.decode(request.getParameter("q"), "UTF-8"));
				
			ArrayList<RodzajZabieguEnt> items = repo.getRodzajeZabiegow();
			

			for (int i = 0; i < items.size(); i++) {
				result += "{ \"value\": \"";
				result += Long.toString(items.get(i).getId());
				result += "\", \"label\": \"";
				result += items.get(i).getNazwa() + " (" + items.get(i).getGabinet().getNumer() 
						+ " " + items.get(i).getGabinet().getNazwa() + ")";
				result += "\"}";
				if(i != items.size()-1)
					result += ",";
				result += System.lineSeparator();
			}
		} else {
			RodzajeWizytRep repo = new RodzajeWizytRep();
			
			if(request.getParameter("q") != null) 
				repo.setSearchText(URLDecoder.decode(request.getParameter("q"), "UTF-8"));
				
			ArrayList<RodzajWizytyEnt> items = repo.getRodzajeWizyt();
			

			for (int i = 0; i < items.size(); i++) {
				result += "{ \"value\": \"";
				result += Long.toString(items.get(i).getId());
				result += "\", \"label\": \"";
				result += items.get(i).getNazwa() + " (" + items.get(i).getGabinet().getNumer() 
						+ " " + items.get(i).getGabinet().getNazwa() + ")";
				result += "\"}";
				if(i != items.size()-1)
					result += ",";
				result += System.lineSeparator();
			}
		}
		

		 //result += "]}";
		result += "]";


		app.OutPrint(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
