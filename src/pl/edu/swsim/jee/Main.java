package pl.edu.swsim.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;


/**
 * Servlet implementation class Main
 */
@WebServlet(description = "Strona startowa", urlPatterns = { "/main", "/Main" })
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		CoreApp app = new CoreApp(this, request, response);
		
		if(!app.isLogedIn()) {
			
			app.FinalRender(
					new Template().setParentServlet(this).useTemplate("main/indexPage", new Assoc[] {}), 
					"Strona główna", 
					new String[] {"Przychodnia","Skróty"}
					);
		} else {
			String userPage = "";
			userPage = new Template().setParentServlet(this).useTemplate("main/mainPage", new Assoc[] {
					
					
			});

			app.FinalRender(userPage, "Strona główna", new String[] {"panele"});
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		app.Redirect("main"); 
	}

}
