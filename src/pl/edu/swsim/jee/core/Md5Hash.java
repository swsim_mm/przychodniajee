package pl.edu.swsim.jee.core;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Hash {
	
	public static String makeMd5Hash(String input) {
		String md5Input = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5Input = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) { }
		return md5Input;
	}

}
