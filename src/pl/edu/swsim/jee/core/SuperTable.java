package pl.edu.swsim.jee.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Klasa rendująca tabelę z opacji sortowania
 * @author Michał
 *
 */
@SuppressWarnings("rawtypes")
public class SuperTable {
	
	private AbstractSuperTableModel model; 
	
	Template template = null; 
	
	// na stałe zdefiniowane względem pozycji w public
	// szablony framgnetów tabeli
	private String tableTemplate = "table/tableFrame";
	
	private String headTemplate = "table/tableHead";
	private String headColumnTemplate = "table/tableHeadColumn";
	
	private String rowTemplate = "table/tableRow";
	private String rowCellTemplate = "table/tableRowCell";
	
	private String sortTargetLink = null;
	private String recordTargetLink = null;
	
	private String extraSortLink = "";
	
	private int sortNum = 0;
		
	public SuperTable() {}

	//----------------------------------------------------------------------
	
	
	public void setModel(AbstractSuperTableModel model) {
		this.model = model;
	}
	public AbstractSuperTableModel getModel() {
		return model;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}


	public void setSortTargetLink(String sortTargetLink) {
		this.sortTargetLink = sortTargetLink;
	}
	public String getSortTargetLink() {
		return sortTargetLink;
	}
	

	public void setRecordTargetLink(String recordTargetLink) {
		this.recordTargetLink = recordTargetLink;
	}
	public String getRecordTargetLink() {
		return recordTargetLink;
	}
	

	public void setSortNum(int sortNum) {
		this.sortNum = sortNum;
	}
	
	
	public int getSortNum() {
		return this.sortNum;
	}

	//----------------------------------------------------------------------

	/**
	 * Zamienia tag na numer sortowania
	 * @param sortTag
	 * @return
	 */
	public int setSortTag(String sortTag) {
		
		if(sortTag == null || !this.TestOrderString(sortTag)) {
			this.sortNum = 0;
			return 0;
		}
		
		String[] parm = this.GetOrderFromString(sortTag);
		
		if(parm == null || !this.isDataName(parm[0])) {
			this.sortNum = 0;
			return 0;
		}
		
		int col = 0;
		for(int i = 0; i< model.getColumnCount(); i++) {
			if(model.getDataName(i).compareToIgnoreCase(parm[0])==0) {
				col = i;
				break;
			}
		}
		
		if(parm[1].compareToIgnoreCase("desc")==0) {
			this.sortNum = (col*2)+1;
			return this.sortNum;
		} else {
			this.sortNum = (col*2);
			return this.sortNum;
		}
		
	}
	
    /**
     * Testuje czy wartość jest nazwą do sortwania
     * @param testedName
     * @return
     */
	private boolean isDataName(String testedName) {
		for(int i = 0; i< model.getColumnCount(); i++)
			if(model.getDataName(i).compareToIgnoreCase(testedName)==0)
				return true;
		return false;
	}
	
	/**
	 * Testuje czy przesłana wartość order pasuje do wzoru
	 * np nazwa_asc, ulica_desc itd
	 * @param order
	 * @return
	 */
	private boolean TestOrderString(String order) {
		return Pattern.matches("^([A-Za-z0-9]+)_(asc|desc)$", order);
	}
	
	/**
	 * Wyciąga informacje order z przesłanego wzoru
	 * np. nazwa_asc -> [0]="nazwa", [1]="asc"
	 * @param order
	 * @return
	 */
	private String[] GetOrderFromString(String order) {
		
		Pattern reg = Pattern.compile("^([A-Za-z0-9]+)_(asc|desc)$");
		Matcher match = reg.matcher(order);
		
		if(match.find()) 	
			return new String[] { match.group(1), match.group(2) };
		else 
			return null;
	}
	
	/**
	 * Generuje wpis linku z bieżącym sortowaniem
	 * automatycznie dodając asc lub desc i nazwę kolumny
	 * @return
	 */
	public String getSortName() {
		int sortColumn = getSortNum()/2;
		boolean asc = (getSortNum()%2) == 0;
		
		return model.getDataName(sortColumn) + "_" + (asc?"asc":"desc");
	}
	
	/**
	 * Doczepia do linku sortowania dodatkowe opcje
	 * @param extraLink
	 */
	public void setExtraLink(String extraLink) {
		this.extraSortLink = extraLink;
	}
	
	public String getExtraLink() {
		return this.extraSortLink;
	}
	
	
	/**
	 * Renduje nagłówek kolumn
	 * @return
	 */
	private String RenderTableHeader() {
				
		String columns = ""; 
		
		int sortColumn = getSortNum()/2; // numer kolumny
		boolean asc = (getSortNum()%2) == 0; // asc czy desc
		
		for(int i = 0; i < model.getColumnCount(); i++) { // kolunmy
			
			String colSortOpt;
			String colIcon = "";
			String colExtraStyle = "";
			
			if(sortColumn == i) { // dla kolumnu objętej sortowaniem dobranie kierunku sortowania
				if(asc) {
					colSortOpt = model.getDataName(i) + "_" + "desc";
					colIcon = "<span class=\"glyphicon glyphicon-triangle-bottom\" aria-hidden=\"true\"></span>";
				} else {
					colSortOpt = model.getDataName(i) + "_" + "asc";
					colIcon = "<span class=\"glyphicon glyphicon-triangle-top\" aria-hidden=\"true\"></span>";
				}
				colExtraStyle += " table-selected-column"; 
			} else {
				colSortOpt = model.getDataName(i) + "_" + "asc";
			}
			
			columns += this.template.useTemplate(headColumnTemplate, new Assoc[] {
					new Assoc("col_num",Integer.toString(i)),
					new Assoc("page",sortTargetLink),
					new Assoc("sort_link",colSortOpt + extraSortLink),
					new Assoc("column_name",model.getColumnName(i)),
					new Assoc("sort_imie_icon",colIcon),
					new Assoc("col_class",colExtraStyle),
			});
		}
		
		// output
		columns = this.template.useTemplate(headTemplate, new Assoc[] {new Assoc("tableHeadColumns",columns)});
				
		return columns;
	}
	
	/**
	 * Renduje ciało tabeli z rekordami
	 * @return
	 */
	private String RenderTableBody() {
		String tableBody = ""; 
		
		for(int i = 0; i < model.getRowCount(); i++) {
			String row = "";
			
			for(int j = 0; j < model.getColumnCount(); j++) {
				
				row += this.template.useTemplate(rowCellTemplate, new Assoc[] {
						new Assoc("row_text",model.getValueAt(i, j)),
				});
			}
			
			tableBody += this.template.useTemplate(rowTemplate, new Assoc[] {
					new Assoc("id",model.getRecordId(i)),
					new Assoc("tableRowCells",row)
			});
			
		}
		
		return tableBody;
	}
	
	/**
	 * Rendowanie całej tabeli (nagłowek kolumn oraz ciało tabeli)
	 * @return
	 */
	public String RenderTable() {
		//return this.RenderTableHeader() + this.RenderTableBody();
		return this.template.useTemplate(this.tableTemplate,new Assoc[] {
				new Assoc("tableHead", this.RenderTableHeader()),
				new Assoc("tableData", this.RenderTableBody()),
		});
	}
	
	/**
	 * Generuje select-option box z wybranym aktualnie sortowaniem
	 * @return
	 */
	public String GenerateSelectSortBox() {
		//String boxCode = "<select id=\"order-cb\" name=\"order\">";
		String boxCode = "";
		
		int sortColumn = getSortNum()/2;
		boolean asc = (getSortNum()%2) == 0;
		
		for(int i = 0; i < model.getColumnCount(); i++) {
			boxCode += "<option value=\""+ model.getDataName(i) +"_asc\"";
			if(i == sortColumn && asc)
				boxCode += " selected";
			boxCode += ">"+ model.getColumnName(i) + " rosnąco [A-Z]</option>";
			
			boxCode += "<option value=\""+ model.getDataName(i) +"_desc\"";
			if(i == sortColumn && !asc)
				boxCode += " selected";		
			boxCode += ">"+ model.getColumnName(i) + " malejąco [Z-A]</option>";
		}
		
		//boxCode += "</select>";
		
		return boxCode;
	}
}