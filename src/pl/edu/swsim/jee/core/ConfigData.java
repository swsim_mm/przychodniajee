package pl.edu.swsim.jee.core;

public class ConfigData {
	private static ConfigData instance = null;
	
	public ConfigData getInstance() {
		if(instance == null) {
			instance = new ConfigData();
		}
		return instance;
	}
	
	public static final String appVersion	= "0.1";
	public static final String appName		= "Przychodnia ŻW";
	public static final String appRootPath 	= "/PrzychodniaJEE/";
	
	public static final String imgPath = appRootPath + "public/";
	
	public static final int sessionTimeOut = 1200; //20min
	
	public static final boolean templateDebug = false;
	
	
}
