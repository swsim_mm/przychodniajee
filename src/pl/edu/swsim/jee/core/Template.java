/**
 * Prosta klasa do obsługi szablonu poprzez replace w servletach
 * 
 * Lepiej było by utworzyć raz jakieś dane w pamięci 
 * z załadowanego szablonu, i używać je wielokrotnie 
 * niż ładować za każdym razem (nie wiem czy nawet
 * getResourceAsStream nie buforuje ich w pamięci)
 * 
 * SWSIM Java EE <-- ciekaw jestem czy google uwieczni ;)
 */

package pl.edu.swsim.jee.core;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;

public class Template {

	private static final String templateDir = "/template/"; // ktalog szablonów w moim "public"
	private static final String templateExt = ".twig"; // rozszerzenie .twig plików
	private static final String templateTwigBegin = "{{ "; // symbol początku pola do zastąpienia	
	private static final String templateTwigEnd = " }}"; // symbol zakończenia/ tego pola
	private static final String templateAnySign = "[.*]"; 
	
	private static Assoc[] staticVar = {
		new Assoc("rootpath", ConfigData.appRootPath),
		new Assoc("rootPath", ConfigData.appRootPath),
		new Assoc("appName", ConfigData.appName),
		new Assoc("appname", ConfigData.appName),
		
	}; // statyczne dane

	private HttpServlet parentServlet = null;
	
	public Template setParentServlet(HttpServlet parent) {
		this.parentServlet = parent;
		return this;
	}
	
	public String useTemplate(HttpServlet parent, String templateName,
			Assoc[] vars) {
		this.parentServlet = parent;
		return this.useTemplate(templateName, vars);
	}
	
	public String useTemplate(String templateName, Assoc[] vars) {

		if(this.parentServlet == null)
			return "Brak definicji parentservlet";
		
		// symbol nowej lini na danej plarformie
		String newline = System.getProperty("line.separator");
		// za pomocą poniższego można wyciągnąć rzeczywistą ścieżkę do zasobów
		// projektu
		// String systemDir = parent.getServletContext().getRealPath("/");

		String result = ""; // resultat szablonu
		String line = ""; // linia która jest wczytywana

		templateName = templateName.trim(); // fix spacji na końcu i początku

		InputStream fis = null;
		InputStreamReader isr = null;
		try {
			// Wczytanie z pliku poprzez zasoby
			fis = parentServlet.getServletContext().getResourceAsStream(
					templateDir + templateName + templateExt);
			// lub
			// InputStream fis = new FileInputStream(systemDir + templateDir
			// + templateName + templateExt);
			// Kodowanie pliku (pracuje tylko na plikach UTF-8
			isr = new InputStreamReader(fis,Charset.forName("UTF-8"));
			
		} catch (Exception e1) {
			System.out.println("Błąd szablonu: \"" + templateDir + templateName
					+ templateExt + "\"");
			fis = null;
			e1.printStackTrace();
		}

		/**
		 * Szablon awaryjny lub wymuszony szablon debugujący
		 */
		if (fis == null || isr == null) {
			result += "<!------------------------------" + templateName + "----------------------------------->";
			result += "<h2>Błąd szablonu: \"" + templateDir + templateName
					+ templateExt + "\"</h2><br />" + CoreApp.newLine;
			
			for (Assoc ass : vars) {
				result += this.SimpleDebugTemplate(ass.name, ass.value);
			}
			
			result += "<!------------------------------" + templateName + "----------------------------------->";
		} 
		else {
			int counter = 0;
			try {
				// Przygotowanie buforowanego źródła
				BufferedReader br = new BufferedReader(isr);

				// Dla każdej linii z szablonu przeszukuję całą tablicę
				// asocjacyjną
				// w poszukiwaniu znaków początku i końca szablonu
				// BTW. znaki {{ tag }} oraz rozszerzenie są zerżnięte z
				// narzedzia TWIG dla php ;)
				while ((line = br.readLine()) != null) {
					// dane przesłane przez parametry
					if(vars != null)
						for (Assoc ass : vars)
							line = line.replaceAll(
									Pattern.quote(templateTwigBegin + ass.name 	+ templateTwigEnd), 
									Matcher.quoteReplacement((ass.value!=null?ass.value:"")));
					
					// dane statyczne wysłane przez aplikccje
					for (Assoc ass : staticVar)
						line = line.replaceAll(
								Pattern.quote(templateTwigBegin + ass.name 	+ templateTwigEnd), 
								Matcher.quoteReplacement(ass.value));
					
					//if(ConfigData.templateDebug) {
						line = line.replaceAll(
								Pattern.quote(templateTwigBegin + templateAnySign + templateTwigEnd), 
								Matcher.quoteReplacement(""));
					//}
					
					result += newline + line;
					counter++;
				}
			} catch (Exception e) {
				System.out.println("-------- AT LINE " + counter + " --------");
				System.out.println(templateDir + templateName + templateExt);
				System.out.println("-----------------------------------------");
				System.out.println(line);
				for (Assoc ass : vars) {
					System.out.println(this.SimpleDebugTemplate(ass.name, ass.value));
				}
				
				
				
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * Proste wypisanie informacji debugujących (klucze oraz wartości)
	 * @param key
	 * @param val
	 * @return
	 */
	private String SimpleDebugTemplate(String key, String val) {
		String result = "";
		
		result += templateTwigBegin + " " + key + " " + templateTwigEnd + CoreApp.newLine;
		result += val + CoreApp.newLine;

		return result;
	}

}
