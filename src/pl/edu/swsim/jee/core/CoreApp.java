package pl.edu.swsim.jee.core;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.zw.dao.UzytkownicyRep;
import pl.edu.swsim.zw.entities.UzytkownikEnt;

public class CoreApp {
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	/**
	 * Z wywołanego kontrolera
	 */
	private HttpServlet parent = null;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;

	public static final String newLine = System.lineSeparator();
	public static final String newLineH = "<br />" + System.lineSeparator();

	/**
	 * Obsługa sesji i authentykacja użytkownika
	 */
	private HttpSession curSession = null;
	private UzytkownikEnt user = null;
	
	private String[] menu = new String[] {
			"panele",
			"przychodnia","wizyty","zabiegi","rozpoznania",
			"dane","pacjenci","pracownicy","uzytkownicy",
			"slowniki","gabinety","choroby","rodzajewizyt","rodzajezabiegow",
			"user",
	};


	public UzytkownikEnt getUser() {
		return this.user;
	}

	public String getSessionNumber() {
		return curSession.getId();
	}
	
	/**
	 * Czy użytkownik jest zalogowany
	 * @return czy jego id jest odczytane z bazy
	 */
	public boolean isLogedIn() {
		return (this.user != null);
	}
	
	/**
	 * 
	 */

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	/**
	 * Inicjowanie aplikacji
	 * 
	 * @param parent
	 *            servlet wywołujący
	 * @param request
	 * @param response
	 */
	public CoreApp(HttpServlet parent, HttpServletRequest request,
			HttpServletResponse response) {
		logger.trace(".");
		this.parent = parent;
		this.request = request;
		this.response = response;
		
		Init();

	}

	/**
	 * Inicjacja i wszystkie pierwsze wartości
	 */
	private void Init() {
		logger.trace(".");
		// zmienia kodowanie na polskieg zanki UTF-8 tak jak w projekcie
		
		// dla post i get data
		try {
			this.request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		this.response.setCharacterEncoding("UTF-8");
		//this.response.setContentType("text/html");

		//this.OutPrint(" jest ok "); return;
		// Sprawdzenie sesji
		this.checkSession();

	}

	/**
	 * Obsługa seji i logowania Umożliwa autentykację za pomocą wcześniej
	 * użuwanej sesji jeśli nie została uniewazniona
	 */
	public void checkSession() {
		logger.trace(".");
		this.curSession = this.request.getSession();
		
		// preset dla nowej sesji
		if (this.curSession.isNew()) {
			logger.trace("Nowa sesja");
			this.curSession.setMaxInactiveInterval(ConfigData.sessionTimeOut); // 20min timemout
			this.user = null;
			return;
		}
		
		// gdy jest już wcześniej używana
		this.user = new UzytkownicyRep().getUserBySession(this.getSessionNumber(), (ConfigData.sessionTimeOut*1000));
		// automatycznie gdy sesja jest ważna przedłuża o kolejny timeout

	}

	/**
	 * Logowanie od systemu
	 * 
	 * @param userName
	 *            nazwa użytkownika
	 * @param password
	 *            hasło do konta
	 * @return czy udał się process logowania
	 * @throws Exception 
	 */
	public boolean Login(String userName, String password) {

		logger.trace(".");
		
		String md5Password = "";
		if(password == null) 
			return false;
		
		md5Password = Md5Hash.makeMd5Hash(password);
				
		this.user = new UzytkownicyRep().confirmCredentials(userName, md5Password);
		
		// nie znaleziono użytkownika
		if(this.user != null) {		
			logger.trace("Użytkownik znaleziony, potwierdzono dane");
			// oznaczenie logowania się użytkownika
			new UzytkownicyRep().logIn(this.user, this.getSessionNumber(), (ConfigData.sessionTimeOut*1000) ).commit();
			this.checkSession();
			return true;
		}

		return false;
		
	}

	/**
	 * Wylogowanie z systemu
	 * 
	 */
	public void Logout() {
		logger.trace(".");
		new UzytkownicyRep().logOut(this.user).commit();
		this.curSession.invalidate();
		this.checkSession();

	}
	
	public void FinalRender(String pageContent, String title, String[] dir) {
		this.FinalRender(pageContent, title, dir, "", "");
	}
	
	public void FinalRender(String pageContent, String title, String[] dir, String header) {
		this.FinalRender(pageContent, title, dir, header, "");
	}

	/**
	 * Generuje finalną stronę użytkownika
	 * @param pageContent
	 * @param title
	 * @param header
	 * @param footer
	 */
	public void FinalRender(String pageContent, String title, String[] dir, String header, String footer) {
		logger.trace(".");
		PrintWriter out = null;
		try {
			out = this.response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		} 
				
		String navigatorSeparator = new Template().useTemplate(parent, "base/navigatorSeparator", null);
		String navBar = "";
		if(dir.length > 0) {
			navBar += " " + dir[0];
			for(int i = 1; i< dir.length;i++) 
				navBar += navigatorSeparator + dir[i];
		}
		
		String menuLinks = "";
		
		if(this.isLogedIn()) {
			footer += new Template().useTemplate(parent, "base/timer", new Assoc[] {});
			
			ArrayList<Assoc> menuSelection = new ArrayList<Assoc>();
			
			Assoc elem;
			for(String menuElement: this.menu) {
				elem = new Assoc("s_" + menuElement, "");
				for(String dirElement: dir) {
					if(menuElement.compareToIgnoreCase(dirElement) == 0)
						elem.value = "active";
				}
				menuSelection.add(elem);
			}
			menuSelection.add(new Assoc("user_full_name",this.user.getPracownik().getImie() 
					+ " " + this.user.getPracownik().getNazwisko()));
			
			Assoc[] assocData = new Assoc[menuSelection.size()];
			assocData = (Assoc[])menuSelection.toArray(assocData);
			
			menuLinks = new Template().useTemplate(parent, "base/menuUser", assocData);
		} else {
			menuLinks = new Template().useTemplate(parent, "base/menuBlank", new Assoc[] {});
		}
		
		if(title.length() > 0)
			title += " - ";

		String finalPage = new Template().useTemplate(this.parent, "main", new Assoc[] {
				new Assoc("header", header), 
				new Assoc("menuLinks", menuLinks), 
				new Assoc("navBar", navBar), 
				new Assoc("preTitle", title),
				new Assoc("pageContent", pageContent),
				//new Assoc("pageContent", pageContent + "" + this.DebugInfo()),
				new Assoc("footer", footer),
				new Assoc("body", "") 
				});
		
		// teraz w stringu mam całą stronę i mogę ją wyświetlić
		out.print(finalPage);

	}
	
	public void RedirectLogin() {
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "login"); 
	}
	
	public void Redirect(String page) {
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + page); 
	}
	
	public void RedirectError() {
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "error"); 
	}
	/**
	 * Na szybko informacje do debugowania
	 * @param out
	 */
	public String DebugInfo() {
		
		String result = "";
		
		Date date = new Date(); // data generowania
		
		result += newLineH + newLineH;
		result += new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		result += newLineH + newLineH;
		result += "User: " + (this.user==null?"null":this.user.getLogin()) + newLineH
				+ "UserId: " + (this.user==null?"null":this.user.getId()) + newLineH
				+ "SessionId: " + this.getSessionNumber() + newLineH;
				result += newLineH + "----------------------------------------------------" + newLineH;
				result += "Cookies:" + newLineH;
				
		Cookie[] cookies = this.request.getCookies();
		if (cookies != null) {
		  for (Cookie ck : cookies) {
			  result += ck.getName() + " : "+ ck.getValue() +  newLineH;
		  }
		}
			
		return result;
	}
	
	public void OutPrint(String outMsg) {
		PrintWriter out = null;
		try {
			out = this.response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		out.print(outMsg);
	}
	
	/**
	 * Sprawdza czy ciąg odpowiada formatowi order
	 * przykład imie_asc, numer_desc  
	 * 
	 * @param order
	 * @return true jeśli format jest poprawny
	 */
	public boolean TestOrderString(String order) {
		return Pattern.matches("^([A-Za-z0-9]+)_(asc|desc)$", order);
	}
	// TODO przesunięte do modelu tabeli
	
	/**
	 * Zwraca tablicę String 2 elementową z formatem order lub null
	 * przykład imie_asc -> [0]imie, [1] asc, numer_desc -> [0]numer, [1]desc
	 * Nazwa pierwszego ciągu jest dowolna [A-Za-z0-1]+ a druga część asc lub desc
	 * @param order
	 * @return null lub tablica 2 elemenowa
	 */
	/*public String[] GetOrderFromString(String order) {
		
		Pattern reg = Pattern.compile("^([A-Za-z0-9]+)_(asc|desc)$");
		Matcher match = reg.matcher(order);
		
		if(match.find()) {
			
			logger.trace("Znaleziono order w stringu " + Integer.toString(match.groupCount()));
			logger.trace("[1] " + match.group(1));
			logger.trace("[2] " + match.group(2));
			
			return new String[] { match.group(1), match.group(2) };
			
		} else {
			return null;
		}
	}*/

}
