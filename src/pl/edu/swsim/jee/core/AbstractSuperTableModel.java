package pl.edu.swsim.jee.core;

import java.util.ArrayList;

/**
 * Abstrakcyjna klasa utworzona na wzór AbstractTableModel
 * @author Michał
 */
public abstract class AbstractSuperTableModel<T> {
		     
	protected ArrayList<T> data; // lista danych
	protected String[] columnNames; // nazwy kolumn widoczne
	protected String[] dataNames; // nazwy słów kluczowych 
	
	
	/**
	 * Liczba wierszy
	 * @return
	 */
    public int getRowCount() {
        if(data==null) return 0;
        return data.size();
    }
    
    /**
     * Liczba kolumn
     * @return
     */
    public int getColumnCount() {
        return columnNames.length;
    }
    
    /**
     * Zawartość komórki
     * @param rowIndex
     * @param columnIndex
     * @return
     */
    public abstract String getValueAt(int rowIndex, int columnIndex);
    
    /**
     * Nazwy kolumn
     * @param column
     * @return
     */
    public String getColumnName(int column) {
        return columnNames[column].toString();
    }
   
    /**
     * Nazwy data dla sortowania
     * @param column
     * @return
     */
    public String getDataName(int column) {
        return dataNames[column].toString();
    }
    
    /**
     * Pobiera id rekodru używane do przenoszenia na stronę edycji itd
     * @param row
     * @return
     */
    public abstract String getRecordId(int row);
    
    /**
     * Ustawienie danych dla modelu
     * @param data
     */
    public void setModelData(ArrayList<T> data) {
	       this.data = data;
	}
    
    /**
     * pobranie rekordu dla wskazanego wiersza
     * @param position
     * @return
     */
    public T getDataRecord(int position) {
        return this.data.get(position);
    }
      
}
