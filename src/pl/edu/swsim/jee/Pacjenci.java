package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.entities.PacjentEnt;

/**
 * Servlet implementation class Pacjenci
 */
@WebServlet(
		description = "Lista pacjentów", 
		urlPatterns = { 
				"/pacjenci", 
				"/Pacjenci"
		})
public class Pacjenci extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pacjenci() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
				
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("pacjenci"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("pacjent"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		PacjenciTableModel tableModel = new PacjenciTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
		
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		PacjenciRep repository = new PacjenciRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
        switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("imie"));
        	break;
        case 1:
        	order.add(Order.desc("imie"));
	        break;
        case 2: 
        	order.add(Order.asc("nazwisko"));
        	break;
        case 3:
        	order.add(Order.desc("nazwisko"));
	        break; 
        case 4: 
        	order.add(Order.asc("m.nazwa"));
        	order.add(Order.asc("u.nazwa"));
        	order.add(Order.asc("w.nazwa"));
        	break;
        case 5:
        	order.add(Order.desc("m.nazwa"));
        	order.add(Order.desc("u.nazwa"));
        	order.add(Order.desc("w.nazwa"));
	        break;
        case 6: 
        	order.add(Order.asc("p.imie"));
        	order.add(Order.asc("p.nazwisko"));
        	break;
        case 7:
        	order.add(Order.desc("p.imie"));
        	order.add(Order.desc("p.nazwisko"));
	        break;  
        }
        
        repository.setOrderBy(order);
        
        // szukany tekst jeśli jest zdefiniowany
        String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }
                
    	tableModel.setModelData(repository.getPacjenci());
		
    	// Rednowanie tekstu tabeli	
		String newTable = superTable.RenderTable();
		
		
		// Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
		String successMsg = "";
		
		if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowego pacjenta: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono pacjenta: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto pacjenta: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć pacjenta: <strong>" + msgText + "</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
		
		// rendowanie tabeli już z nagłówkiem i zawartością		
		newTable = new Template().setParentServlet(this).useTemplate("pacjenci/overviewTable", new Assoc[] {
				new Assoc("tableContent",newTable), 
				new Assoc("searchText",searchText), 
				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
				new Assoc("orderLink", superTable.getSortName()),
				new Assoc("message", successMsg),
		});
		
		// finałowe renderowanie strony
		app.FinalRender(newTable, "Lista pacjentów", new String[] {"dane", "pacjenci"});
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ta strona nie lubi post ;)
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "pracownicy"); 
	}
	
	/**
	 * Model danych tabeli
	 */
	public class PacjenciTableModel extends AbstractSuperTableModel<PacjentEnt> {
		 	
		//private ArrayList<PracownikEnt> data = null; // lista danych

	    private final static int IMIE_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int NAZWISKO_IDX = 1;
	    private final static int ADRES_IDX = 2;
	    private final static int LEKARZ_IDX = 3;
	 
	    public PacjenciTableModel() {
			columnNames = new String[] {"Imię", "Nazwisko", "Adres", "Lekarz"}; // nazwy w Header
			dataNames = new String[] {"imie", "nazwisko", "adres", "lekarz"}; // nazwy dla sort
			// Z tych danych będzie utworzny select box do sortowania !
	    }
	    
	    // wyciąganie danych z listy
	    public String getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        PacjentEnt item = data.get(rowIndex);
	        switch (columnIndex) {
	            case IMIE_IDX:
	                return item.getImie();
	            case NAZWISKO_IDX:
	                return item.getNazwisko();
	            case ADRES_IDX:
	                return (
	                		item.getUlica().getMiasto().getNazwa() 
	                		+ " " + item.getUlica().getMiasto().getKod_pocztowy() 
	                		+ " (" + item.getUlica().getMiasto().getWojewodztwo().getNazwa() + ")"
	                		+ ", </br>" + item.getUlica().getNazwa() 
	                		+ " " + item.getBudynek() 
	                		+ " " + item.getMieszkanie());
	            case LEKARZ_IDX:
	                return (item.getPracownik()!=null?(item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko()):"brak");
	            default:
	                return "";
	        }
	    }
	    
	    // wyciaganie id rekodru po numerze w liście
		public String getRecordId(int row) {
			return Long.toString(data.get(row).getId());
		}
	    
		// reszta załatwia klasa tabeli
	}

}
