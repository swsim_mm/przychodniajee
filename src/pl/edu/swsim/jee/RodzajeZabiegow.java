package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.RodzajeZabiegowRep;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;

/**
 * Servlet implementation class RodzajeZabiegow
 */
@WebServlet(
		description = "Lista rodzajów wizyt", 
		urlPatterns = { 
				"/rodzajezabiegow", 
				"/RodzajeZabiegow"
		})
public class RodzajeZabiegow extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RodzajeZabiegow() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("rodzajezabiegow"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("rodzajzabiegu"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		RodzajeZabiegowTableModel tableModel = new RodzajeZabiegowTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
				
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		RodzajeZabiegowRep repository = new RodzajeZabiegowRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
		switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("nazwa"));
        	break;
        case 1:
        	order.add(Order.desc("nazwa"));
	        break;
        case 2: 
        	order.add(Order.asc("rodzaj"));
        	break;
        case 3:
        	order.add(Order.desc("rodzaj"));
	        break;  
        case 4: 
        	order.add(Order.asc("cena"));
        	break;
        case 5:
        	order.add(Order.desc("cena"));
	        break; 
        case 6: 
        	order.add(Order.asc("refundacja"));
        	break;
        case 7:
        	order.add(Order.desc("refundacja"));
	        break;
        case 8: 
        	order.add(Order.asc("punkty"));
        	break;
        case 9:
        	order.add(Order.desc("punkty"));
	        break;
        case 10: 
        	order.add(Order.asc("skierowanie"));
        	break;
        case 11:
        	order.add(Order.desc("skierowanie"));
	        break;
		}
		
		repository.setOrderBy(order);
		
		// szukany tekst jeśli jest zdefiniowany
        String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }
        
        tableModel.setModelData(repository.getRodzajeZabiegow());
        
     // Rendowanie tekstu tabeli	
     	String newTable = superTable.RenderTable();
     	
     // Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
     	String successMsg = "";
     	
     	if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowy rodzaj zabiegu: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono dane rodzaju zabiegu: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto rodzaj zabiegu: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć rodzaju zabiegu: <strong>" + msgText + "</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
     	
     // rendowanie tabeli już z nagłówkiem i zawartością		
     		newTable = new Template().setParentServlet(this).useTemplate("rodzajezabiegow/overviewTable", new Assoc[] {
 				new Assoc("tableContent",newTable), 
 				new Assoc("searchText",searchText), 
 				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
 				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
 				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
 				new Assoc("orderLink", superTable.getSortName()),
 				new Assoc("message", successMsg),
     		});
		
 		// finałowe renderowanie strony
		app.FinalRender(newTable, "Lista rodzajów zabiegow", new String[] {"slowniki", "rodzajezabiegow"});		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "rodzajezabiegow"); 
		
	}
	
	/**
	 * Model danych tabeli
	 */
	public class RodzajeZabiegowTableModel extends AbstractSuperTableModel<RodzajZabieguEnt> {
		 	
	
		private final static int NAZWA_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int RODZAJ_IDX = 1;
	    private final static int CENA_IDX = 2;
	    private final static int REFUNDACJA_IDX = 3;
	    private final static int PUNKTY_IDX = 4;
	    private final static int SKIEROWANIE_IDX = 5;
	    private final static int CZASTRWANIA_IDX = 6;
	
		 
		    public RodzajeZabiegowTableModel() {
				columnNames = new String[] {"Nazwa", "Rodzaj", "Cena", "Refundacja", "Punkty", "Skierowanie", "Czas trwania"}; // nazwy w Header
				dataNames = new String[] {"nazwa", "rodzaj", "cena", "refundacja", "punkty", "skierowanie", "czas"}; // nazwy dla sort
				// Z tych danych będzie utworzny select box do sortowania !
		    }
		    
		    // wyciąganie danych z listy
		    public String getValueAt(int rowIndex, int columnIndex) {
		 
		    	 if(data == null) return null;
			        RodzajZabieguEnt item = data.get(rowIndex);
		        switch (columnIndex) {
		            case NAZWA_IDX:
		                return item.getNazwa();
		            case RODZAJ_IDX:
		                return item.getRodzaj();
		            case CENA_IDX:
		            	// TODO
		            	return String.valueOf(item.getCena());	
		            case REFUNDACJA_IDX:
		            	return (item.isRefundacja()?"Tak":"Nie");
		            case PUNKTY_IDX:
		            	return Integer.toString(item.getPunkty());
		            case SKIEROWANIE_IDX:
		            	return (item.isSkierowanie()?"Tak":"Nie");
		            case CZASTRWANIA_IDX:
		            	Calendar cal = Calendar.getInstance();
		        		cal.setTime(item.getCzastrwania());
		            	return (Integer.toString(cal.get(Calendar.HOUR_OF_DAY))
		            			+ " godz. "
		            			+ Integer.toString(cal.get(Calendar.MINUTE))
		            			+ " min."
		            			);
		            default:
		                return "";
		        }
		    }
		    
		    // wyciaganie id rekodru po numerze w liście
			public String getRecordId(int row) {
				return Long.toString(data.get(row).getId());
			}
		    
			// reszta załatwia klasa tabeli
		}
	
}
