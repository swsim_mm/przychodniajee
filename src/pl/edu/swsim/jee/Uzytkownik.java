package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Md5Hash;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.dao.UzytkownicyRep;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.UzytkownikEnt;

/**
 * Servlet implementation class Uzytkownik
 */
@WebServlet(
		description = "edycja uzytkownika", 
		urlPatterns = { 
				"/uzytkownik", 
				"/Uzytkownik"
		})
public class Uzytkownik extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
	// Wszystkie pola które mają być przechwycone z postData
	private static String[] formFields = new String[] {
		"id", "login", "haslo", "pracownik", 
	};
	// Strona do której mozna przekierować w razie małych 
	// błedów (danych wpowadzonych przez url)
	public static final String TABLE_PAGE = "uzytkownicy"; 
	public static final String FORM_PAGE = "uzytkownik"; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Uzytkownik() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// gdzie iść przeniesnione w "mode"
		if(request.getParameter("mode") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 

		String formMode = request.getParameter("mode");
		
		/******************************************************
		 * Dla new nie trzeba niczego pobierać na starcie
		 */
		if(formMode.compareToIgnoreCase("new") == 0) {
			NewItem(app, new UzytkownikEnt());
			return;
		}
		
		// dla reszty będzie trzeba pobrać id rekordu
		if(request.getParameter("id") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 
		String strId = request.getParameter("id");
		Long id;
					
		// Próba konwersji tekstu otrzymanego z url na liczbę long
		try {
			id = Long.parseLong(strId);
			if(id < 0)
				throw new Exception("Zły numer id");
		} catch(Exception e) {
			logger.trace(TextTools.twoLine("Błąd parsowania id"));
			app.Redirect(TABLE_PAGE); // taka pomyłka może wyniknać tylko z ingerencji adresem
			
			return;
		}
		
		/******************************************************
		 * Pobranie użytkownika	
		 */	
		ArrayList<UzytkownikEnt> data = new UzytkownicyRep().setId(id).getUzytkownicy();
		
		// ostatni raz opusczenie strony gdy coś będzie nie tak
		if(data.size() != 1) {
			logger.trace(TextTools.twoLine("Błąd pobierania rekordu po jego id"));
			app.RedirectError(); 
			return;
		} 
		
		UzytkownikEnt item = data.get(0);
		
		if(item.isAdministrator() && !app.getUser().isAdministrator()) {

			app.Redirect(TABLE_PAGE + "?msg=admin&text=");
		}
		
		
		// Mając użytkownika można wywołac pożądaną akcję
		if(formMode.compareToIgnoreCase("view") == 0) 
			ViewItem(app, item);
		else if(formMode.compareToIgnoreCase("edit") == 0) 
			EditItem(app, item); 
		else if(formMode.compareToIgnoreCase("remove") == 0) 
			RemoveItem(app, item);
		else 
			app.Redirect(TABLE_PAGE);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// co będzie się działo zdefinowane w mode
		String formMode = request.getParameter("mode");
		
		// Mając użytkownika można wywołac pożądaną akcję
		// null wymusza pobranie danych z formatek
		if(formMode.compareToIgnoreCase("new") == 0)  {
			NewItem(app, null);
		} else if(formMode.compareToIgnoreCase("view") == 0)  {
			ViewItem(app, null);
		} else if(formMode.compareToIgnoreCase("edit") == 0) {
			EditItem(app, null);
		} else if(formMode.compareToIgnoreCase("remove") == 0) {
			RemoveItem(app, null);
		} else {
			app.Redirect(TABLE_PAGE);
		}
	}

	
	
	/**
	 * Pobiera dane z formatki i zapisuje je w rekordzie 
	 * @param request
	 * @param validResult
	 * @return
	 */
	private UzytkownikEnt getDataResults(HttpServletRequest request, boolean[] validResult) {	
		logger.trace(".");
		boolean isOK = true; // gdy nie wychwycono znaczących błędów
		
		Map<String, String> postData = new HashMap<String, String>();
		
		// pobranie elementów formularza z request
		for(String field: formFields) {
			postData.put(field, request.getParameter(field));		
		}
		
		UzytkownikEnt item = new UzytkownikEnt();
		
		// id gdy edytowany jest istniejący użytkownik
		if(postData.get("id") != null) {
			logger.trace(TextTools.twoLine("ID (string):" + postData.get("id")));
			try { //trochę zachodu z konwersją
				Long id = Long.parseLong(postData.get("id"));
				if(id > 0) {
					item.setId(id); 
					logger.trace(TextTools.twoLine("ID:" + Long.toString(id)));
				}
			} catch (Exception e) { }
		}
		
		/*	"id", "login", "haslo", "pracownik", 
		 * */
		
		item.setLogin(postData.get("login"));
		
		
		
		if(item.getId() > 0 && ( postData.get("haslo") == null ||  postData.get("haslo").length() < 1)) {
			ArrayList<UzytkownikEnt> users = new UzytkownicyRep().setId(item.getId()).getUzytkownicy();
			if(users.size() == 1)
				item.setHaslo(users.get(0).getHaslo());
		} else {
			item.setHaslo(Md5Hash.makeMd5Hash(postData.get("haslo")));
		}
			
		if(postData.get("pracownik") != null && postData.get("pracownik").length() > 0 
				&& postData.get("pracownik").compareToIgnoreCase("0") != 0) {
			try {
				Long prId = Long.parseLong(postData.get("pracownik"));
				
				ArrayList<PracownikEnt> prList = new PracownicyRep().setId(prId).getPracownicy();
				if(prList.size() == 1)
					item.setPracownik(prList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}

		
				
		validResult[0] = isOK; // java nie przekazuje referencji dla typów prymitywnych, stąd tablica
		return item;
	}
	
	/**
	 * Renderuje pola formatki
	 * @param render
	 * @param item
	 * @return
	 */
	private String RenderForm(Template render, UzytkownikEnt item) {
		logger.trace(".");
		
		// Adres może nie mieć wybrane województwa, lepiej sie upewnić
		Long idPracownik = 0L;
		if(item.getPracownik() != null)
			idPracownik = item.getPracownik().getId();
		
		// generowanie select-option z wybraym przypisanym pracownikiem
		String pracownicy = "";
		ArrayList<PracownikEnt> pracownicyList = new PracownicyRep().getPracownicy();
		for(int i = 0; i<pracownicyList.size(); i++) 
		{
			pracownicy += "<option value=\"" + pracownicyList.get(i).getId() + "\"";
			if(pracownicyList.get(i).getId() == idPracownik)
				pracownicy += " selected";
			pracownicy += ">";
			pracownicy += pracownicyList.get(i).getImie() + " " + pracownicyList.get(i).getNazwisko();
			pracownicy += "</option>";
		}
		
		// zbieranie danych do formatki
		ArrayList<Assoc> parm = new ArrayList<Assoc>();
				
		parm.add(new Assoc("id",(Long.toString(item.getId()))));
		parm.add(new Assoc("login",(item.getLogin())));
		parm.add(new Assoc("haslo",""));

		parm.add(new Assoc("pracownik",pracownicy));
				
		Assoc[] assocData = new Assoc[parm.size()];
		assocData = parm.toArray(assocData);

		return render.useTemplate(this, "uzytkownicy/uzytkownikForm", assocData);
	}
	
	/**
	 * Akcja nowego elementu
	 * @param app
	 * @param item
	 */
	private void NewItem(CoreApp app, UzytkownikEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("New Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		// strona nie jest wyśietlana pierwszy raz, dane zostały przekazane przez formularz
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false; 
		}
		
		Template templ = new Template().setParentServlet(this);
		
		// trzeba wyświetlić formularz
		if(firstRender || !isOK[0]) {
			
			String preFrom = templ.useTemplate("form/formNewBegin", new Assoc[] {
					new Assoc("formTitle","Nowy użytkownik"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formNewEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Dodaj użytkowika", new String[] {"dane", "uzytkownicy", "Nowy użytkownik"});
			
		} else {
			// nie ma błędów, można wykonać akcję
			new UzytkownicyRep().setUzytkownik(item).AddUzytkownik();
			app.Redirect(TABLE_PAGE + "?msg=new&text=" + item.getLogin() + " - " + item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko());
			
		}
		logger.trace(TextTools.oneLine("New Item end"));

	}
	
	private void EditItem(CoreApp app, UzytkownikEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("EditItem"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
			
		Template templ = new Template().setParentServlet(this);
		
		if(firstRender || !isOK[0]) {

			String preFrom = templ.useTemplate("form/formEditBegin", new Assoc[] {
					new Assoc("formTitle","Edytuj uzytkownika"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formEditEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Edytuj użytkownika", new String[] {"dane", "uzytkownicy", "Edytuj użytkownika"});
			
		} else {
						
			new UzytkownicyRep().setUzytkownik(item).EditUzytkownik();
			app.Redirect(TABLE_PAGE + "?msg=edit&text=" + item.getLogin() + " - " 
					+ item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko());
			
		}
		logger.trace(TextTools.oneLine("EditItem end"));
	}
	

	
	private void ViewItem(CoreApp app, UzytkownikEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("ViewItem"));
				
		if(item == null) {
			app.RedirectError();
		}
			
		Template templ = new Template().setParentServlet(this);
		
		String preFrom = templ.useTemplate("form/formViewBegin", new Assoc[] {
				new Assoc("formTitle","Podgląd uzytkownika"),
				new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
		});
		
		String postForm = templ.useTemplate("form/formViewEnd", new Assoc[] {});
		
		String form = this.RenderForm(templ,item);
					
		app.FinalRender(preFrom + form + postForm, "Podgląd użytkownika", new String[] {"dane", "uzytkownicy", "Podgląd uzytkownika"});
			

		logger.trace(TextTools.oneLine("ViewItem end"));
		
	}
	
	private void RemoveItem(CoreApp app, UzytkownikEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("Remove Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
		
		Template templ = new Template().setParentServlet(this);
		
		
		if(firstRender) {

			String removeForm = templ.useTemplate("form/formRemove", new Assoc[] {
					new Assoc("formTitle","Usuń uzytkownika"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
					new Assoc("message","Czy napewno chcesz ususnąć użytkownika <strong>" + item.getLogin() 
							+ " - " +  item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko() + "</strong>?"),
					new Assoc("warning", templ.useTemplate("uzytkownicy/removeWarning", new Assoc[]{}))
			});
						
			app.FinalRender(removeForm, "Usuń pracownika", new String[] {"dane", "uzytkownicy", "Usuń użytkownika"});
			
		} else {
			
			logger.trace(item.toString());
			
			Long id = item.getId();
			
			// dociągnięcie danych pracownika
			ArrayList<UzytkownikEnt> items = new UzytkownicyRep().setId(id).getUzytkownicy();
 			if(items.size() != 1)
 				app.RedirectError();
 			
 			item = items.get(0); // teraz mamy dane takie jak imie i nazwisko
			
			String msg = item.getLogin() + " - " + item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko();
			
			new UzytkownicyRep().setUzytkownik(item).DeleteUzytkownik(); // usunięcie 
			
			if(new UzytkownicyRep().setId(id).getUzytkownicy().size() == 0) // sprawdzenie czy się udało usunąć
				app.Redirect(TABLE_PAGE + "?msg=remove&text=" + msg);
			else
				app.Redirect(TABLE_PAGE + "?msg=fail&text=" + msg);
	
		}
		logger.trace(TextTools.oneLine("Remove end"));
	}
}
