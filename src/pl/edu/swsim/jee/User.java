package pl.edu.swsim.jee;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Md5Hash;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.UzytkownicyRep;
import pl.edu.swsim.zw.entities.PracownikEnt;

/**
 * Servlet implementation class User
 */
@WebServlet(
		description = "strona użytkownika na której moze zmienić swoje dane", 
		urlPatterns = { 
				"/user", 
				"/User"
		})
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		finalRender(app, null);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		/*
		 * Post tylko gdy użytkownik wysłał chęć zmiany hasła 
		 */
		
		String stareHaslo, noweHaslo, powtorzHaslo;
		
		stareHaslo = request.getParameter("starehaslo");
		noweHaslo = request.getParameter("nowehaslo");
		powtorzHaslo = request.getParameter("powtorzhaslo");
		
		// jedno z pól pozostało nieuzupełnione
		if(stareHaslo == null || stareHaslo.length() == 0 
				|| noweHaslo == null || noweHaslo.length() == 0 
				|| powtorzHaslo == null || powtorzHaslo.length() == 0) {
			String wiadomosc = new Template().setParentServlet(this).useTemplate("base/msgFail", new Assoc[]{
					new Assoc("message", "Aby zmienić hasło należy uzupełnić wszystkie pola."),
			});
			finalRender(app, wiadomosc);
			return;
		}
		
		// jedno z pól pozostało nieuzupełnione
		if(noweHaslo.compareTo(powtorzHaslo) != 0) {
			String wiadomosc = new Template().setParentServlet(this).useTemplate("base/msgFail", new Assoc[]{
					new Assoc("message", "Nowe hasło nie jest identyczne z powtórzonym."),
			});
			finalRender(app, wiadomosc);
			return;
		}
		
		UzytkownicyRep uRep = new UzytkownicyRep();
		uRep = uRep.changePassword(app.getUser(), Md5Hash.makeMd5Hash(stareHaslo), Md5Hash.makeMd5Hash(noweHaslo));
		
		if(uRep == null) {
			String wiadomosc = new Template().setParentServlet(this).useTemplate("base/msgFail", new Assoc[]{
					new Assoc("message", "Podane hasło jest niepoprawne."),
			});
			finalRender(app, wiadomosc);
		} else {
			String wiadomosc = new Template().setParentServlet(this).useTemplate("base/msgSuccess", new Assoc[]{
					new Assoc("message", "Hasło zostało pomyślnie zmienione."),
			});
			finalRender(app, wiadomosc);
		}
		
		

	}
	
	private void finalRender(CoreApp app, String msg) {
		
		String message = "";
		
		if(msg != null)
			message = msg;
		
		String userInfo = "";
		PracownikEnt pracownik = app.getUser().getPracownik();
		userInfo += pracownik.getImie() + " " + pracownik.getNazwisko() + CoreApp.newLineH;
		if(pracownik.getUlica() != null) {
			userInfo += pracownik.getUlica().getNazwa() + " " + pracownik.getBudynek() + " " + pracownik.getMieszkanie() + CoreApp.newLineH;
			userInfo += pracownik.getUlica().getMiasto().getNazwa() + " " + pracownik.getUlica().getMiasto().getKod_pocztowy() + CoreApp.newLineH;
			userInfo += pracownik.getUlica().getMiasto().getWojewodztwo().getNazwa() + CoreApp.newLineH;
		}
		
		String userPage = "";
		
		userPage = new Template().setParentServlet(this).useTemplate("main/userPage", new Assoc[] {
				new Assoc("fullUser", app.getUser().getPracownik().getImie() + " " + app.getUser().getPracownik().getNazwisko()),
				new Assoc("user", app.getUser().getLogin()),
				new Assoc("userInfo", userInfo),
				new Assoc("userLogInfo", (app.getUser().getLast_visit()!=null?new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(app.getUser().getLast_visit()):"brak")),
				new Assoc("message", message),
		});
		
		app.FinalRender(userPage, "Moje konto", new String[] {"user"});
	}

}
