package pl.edu.swsim.jee;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.dao.RodzajeWizytRep;
import pl.edu.swsim.zw.dao.RodzajeZabiegowRep;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Servlet implementation class Termin
 */
@WebServlet(
		description = "Ustalanie terminu zabiegu lub wizyty", 
		urlPatterns = { 
				"/termin", 
				"/Termin"
		})
public class Termin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
	// Wszystkie pola które mają być przechwycone z postData
		private static String[] formFields = new String[] {
			"id", "typ", "typterminu", "rodzaj", 
			"lekarz", "pacjent-text", "pacjent-id",
			"refundacja", "obecny", "date-text", "czas-h", "czas-m",
			"dodatkowe",
		};
		// Strona do której mozna przekierować w razie małych 
		// błedów (danych wpowadzonych przez url)
		public static final String TABLE_PAGE = "terminy"; 
		public static final String FORM_PAGE = "termin";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Termin() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// gdzie iść przeniesnione w "mode"
		if(request.getParameter("mode") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		}
		
		String formMode = request.getParameter("mode");
		
		/******************************************************
		 * Dla new nie trzeba niczego pobierać na starcie
		 */
		if(formMode.compareToIgnoreCase("new") == 0) {
			NewItem(app, new TerminEnt());
			return;
		}
		
		// dla reszty będzie trzeba pobrać id rekordu
		if(request.getParameter("id") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 
		String strId = request.getParameter("id");
		Long id;
		
		// Próba konwersji tekstu otrzymanego z url na liczbę long
		try {
			id = Long.parseLong(strId);
			if(id < 0)
				throw new Exception("Zły numer id");
		} catch(Exception e) {
			logger.trace(TextTools.twoLine("Błąd parsowania id"));
			app.Redirect(TABLE_PAGE); // taka pomyłka może wyniknać tylko z ingerencji adresem
			
			return;
		}
			
		/******************************************************
		 * Pobranie użytkownika	
		 */	
		ArrayList<TerminEnt> data = new TerminyRep().setId(id).getTerminy();
		
		// ostatni raz opusczenie strony gdy coś będzie nie tak
		if(data.size() != 1) {
			logger.trace(TextTools.twoLine("Błąd pobierania rekordu po jego id"));
			app.RedirectError(); 
			return;
		} 
			
		TerminEnt item = data.get(0);
		
		// Mając rekord można wywołac pożądaną akcję
		if(formMode.compareToIgnoreCase("view") == 0) 
			ViewItem(app, item);
		else if(formMode.compareToIgnoreCase("edit") == 0) 
			EditItem(app, item); 
		else if(formMode.compareToIgnoreCase("remove") == 0) 
			RemoveItem(app, item);
		else 
			app.Redirect(TABLE_PAGE);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// co będzie się działo zdefinowane w mode
		String formMode = request.getParameter("mode");
		
		// Mając użytkownika można wywołac pożądaną akcję
		// null wymusza pobranie danych z formatek
		if(formMode.compareToIgnoreCase("new") == 0)  {
			NewItem(app, null);
		} else if(formMode.compareToIgnoreCase("view") == 0)  {
			ViewItem(app, null);
		} else if(formMode.compareToIgnoreCase("edit") == 0) {
			EditItem(app, null);
		} else if(formMode.compareToIgnoreCase("remove") == 0) {
			RemoveItem(app, null);
		} else {
			app.Redirect(TABLE_PAGE);
		}
	}
	

	/**
	 * Pobiera dane z formatki i zapisuje je w rekordzie 
	 * @param request
	 * @param validResult
	 * @return
	 */
	private TerminEnt getDataResults(HttpServletRequest request, boolean[] validResult) {	
		logger.trace(".");
		boolean isOK = true; // gdy nie wychwycono znaczących błędów
		
		Map<String, String> postData = new HashMap<String, String>();
		
		// pobranie elementów formularza z request
		for(String field: formFields) {
			postData.put(field, request.getParameter(field));		
		}
		
		TerminEnt item = new TerminEnt();
		
		// id gdy edytowany jest istniejący 
		if(postData.get("id") != null) {
			logger.trace(TextTools.twoLine("ID (string):" + postData.get("id")));
			try { //trochę zachodu z konwersją
				Long id = Long.parseLong(postData.get("id"));
				if(id > 0) {
					item.setId(id); 
					logger.trace(TextTools.twoLine("ID:" + Long.toString(id)));
				}
			} catch (Exception e) { }
		}
				
		if(postData.get("typterminu")!=null) {
			if(postData.get("typterminu").compareToIgnoreCase("zabieg")==0) 
				item.setZabieg(true);
			else 
				item.setZabieg(false);
		}
		
		Long rodzajId = 0L;
		if(postData.get("rodzaj") != null) {
			logger.trace(TextTools.twoLine("rodzaj ID (string):" + postData.get("rodzaj")));
			try { 
				rodzajId = Long.parseLong(postData.get("rodzaj"));
			} catch (Exception e) { 
				isOK = false;
			}
		}
		
		if(item.isZabieg()) {
			ArrayList<RodzajZabieguEnt> rodzZab = new RodzajeZabiegowRep().setId(rodzajId).getRodzajeZabiegow();
			if(rodzZab.size() == 1) {
				item.setRodzajzabiegu(rodzZab.get(0));
			}
		} else {
			ArrayList<RodzajWizytyEnt> rodzWiz = new RodzajeWizytRep().setId(rodzajId).getRodzajeWizyt();
			if(rodzWiz.size() == 1) {
				item.setRodzajwizyty(rodzWiz.get(0));
			}
		}
		
		if(postData.get("lekarz") != null && postData.get("lekarz").compareToIgnoreCase("0") != 0) {
			try {
				Long prId = Long.parseLong(postData.get("lekarz"));
				
				ArrayList<PracownikEnt> prList = new PracownicyRep().setId(prId).getPracownicy();
				if(prList.size() == 1)
					item.setPracownik(prList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
		
		if(postData.get("pacjent-id") != null && postData.get("pacjent-id").compareToIgnoreCase("0") != 0) {
			try {
				Long paId = Long.parseLong(postData.get("pacjent-id"));
				
				ArrayList<PacjentEnt> paList = new PacjenciRep().setId(paId).getPacjenci();
				if(paList.size() == 1)
					item.setPacjent(paList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
		
		Calendar cal = Calendar.getInstance();
		
		if(postData.get("czas-h") != null || postData.get("czas-m") != null){

			if(Integer.parseInt(postData.get("czas-h")) < 24 
					&& Integer.parseInt(postData.get("czas-h")) >= 0) {
				cal.set(Calendar.HOUR_OF_DAY, (Integer.valueOf(postData.get("czas-h"))));
			} else {
				cal.set(Calendar.HOUR_OF_DAY, 0);
				isOK = false;
			}
			
			if(Integer.parseInt(postData.get("czas-m")) < 60 
					&& Integer.parseInt(postData.get("czas-m")) >= 0) {
				cal.set(Calendar.MINUTE, (Integer.valueOf(postData.get("czas-m"))));
			} else {
				cal.set(Calendar.MINUTE, 0);
				isOK = false;
			}
		}
		
		logger.trace(TextTools.twoLine("Data"));
		if(postData.get("date-text") != null ){
			String dateText = postData.get("date-text");
	        String[] dateData = dateText.split("\\.");
	        
	
	        if(dateData.length != 3) {
	    		dateText = todayDate();
	    		dateData = dateText.trim().split("\\.");
	    	}
	    	
	    	logger.trace(Integer.parseInt(dateData[0]) + " " + Integer.parseInt(dateData[1]) + " " + Integer.parseInt(dateData[2]));
	    
	    	cal.set(Calendar.YEAR, Integer.parseInt(dateData[0]));
	    	cal.set(Calendar.MONTH, Integer.parseInt(dateData[1])-1);
	    	cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateData[2]));
		}
		
		item.setTermin(cal.getTime());
		
		if(postData.get("dodatkowe") != null ){
			item.setDodatkowe(postData.get("dodatkowe"));
		}
				
						
		validResult[0] = isOK; // java nie przekazuje referencji dla typów prymitywnych, stąd tablica
		return item;
	}
	
	
	/**
	 * Renderuje pola formatki
	 * @param render
	 * @param item
	 * @return
	 */
	private String RenderForm(Template render, TerminEnt item) {
		logger.trace(".");
		
		
		// zbieranie danych do formatki
		ArrayList<Assoc> parm = new ArrayList<Assoc>();
				
		parm.add(new Assoc("id",(Long.toString(item.getId()))));
		
		if(item.isZabieg()) {
			parm.add(new Assoc("s-zabieg", "active"));
			parm.add(new Assoc("s-wizyta", ""));
			parm.add(new Assoc("typterminu", "zabieg"));
			
			String selList = "";
			ArrayList<RodzajZabieguEnt> zabiegi = new RodzajeZabiegowRep().getRodzajeZabiegow();
			for(RodzajZabieguEnt rz: zabiegi) {
				selList += "<option value=\"" + Long.toString(rz.getId()) + "\"";
				if(item.getRodzajzabiegu() != null && rz.getId() == item.getRodzajzabiegu().getId())
					selList += " selected";
				selList += ">" + rz.getNazwa() + " (" + rz.getGabinet().getNumer() 
						+ " " + rz.getGabinet().getNazwa() + ")</option>";
			}
			
			parm.add(new Assoc("rodzaj", selList));
			
			
		} else { 
			parm.add(new Assoc("s-zabieg", ""));
			parm.add(new Assoc("s-wizyta", "active"));
			parm.add(new Assoc("typterminu", "wizyta"));
			
			String selList = "";
			ArrayList<RodzajWizytyEnt> wizyty = new RodzajeWizytRep().getRodzajeWizyt();
			for(RodzajWizytyEnt rw: wizyty) {
				selList += "<option value=\"" + Long.toString(rw.getId()) + "\"";
				if(item.getRodzajwizyty() != null && rw.getId() == item.getRodzajwizyty().getId())
					selList += " selected";
				selList +=  ">" + rw.getNazwa() + " (" + rw.getGabinet().getNumer() 
						+ " " + rw.getGabinet().getNazwa() + ")</option>";
			}
			
			parm.add(new Assoc("rodzaj", selList));
		}
		
		// Lekarz 
		Long idLekarz = -1L;
		if(item.getPracownik() != null)
			idLekarz = item.getPracownik().getId();
		
		// generowanie select-option z wybraym przypisanym lekarzem
		String lekarze = "";
		ArrayList<PracownikEnt> lekarzeList = new PracownicyRep().setRodzajPracownika(PracownikEnt.TYP_LEKARZ).getPracownicy();
		for(int i = 0; i<lekarzeList.size(); i++) 
		{
			lekarze += "<option value=\"" + lekarzeList.get(i).getId() + "\"";
			if(lekarzeList.get(i).getId() == idLekarz)
				lekarze += " selected";
			lekarze += ">";
			lekarze += lekarzeList.get(i).getImie() + " " + lekarzeList.get(i).getNazwisko();
			lekarze += "</option>";
		}
		
		parm.add(new Assoc("lekarze",lekarze));
		
				
		if(item.getTermin() == null) {
			parm.add(new Assoc("czas-h","0"));
			parm.add(new Assoc("czas-m","0"));
			parm.add(new Assoc("date-text",this.todayDate()));
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTime(item.getTermin());
			
			parm.add(new Assoc("czas-h",Integer.toString(cal.get(Calendar.HOUR_OF_DAY))));
			parm.add(new Assoc("czas-m",Integer.toString(cal.get(Calendar.MINUTE))));
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");	        		
			parm.add(new Assoc("date-text",format1.format( cal.getTime() )));
		}
			
		
		if(item.getPacjent() != null) {
			parm.add(new Assoc("pacjent-id", Long.toString(item.getPacjent().getId())));
			
			String pacjentText = "";
			
			pacjentText += item.getPacjent().getImie() + " " +item.getPacjent().getNazwisko();
			pacjentText += " - " + item.getPacjent().getUlica().getNazwa() + " " + item.getPacjent().getBudynek() + " " 
					+ item.getPacjent().getMieszkanie() + ", " + item.getPacjent().getUlica().getMiasto().getNazwa() 
					+ " " +item.getPacjent().getUlica().getMiasto().getKod_pocztowy() + ", " 
					+ item.getPacjent().getUlica().getMiasto().getWojewodztwo().getNazwa(); 
			
			parm.add(new Assoc("pacjent-text",pacjentText));
			
		} else {
			parm.add(new Assoc("pacjent-id", ""));
			parm.add(new Assoc("pacjent-text"," "));
		}
		
		if(item.getDodatkowe() != null)
			parm.add(new Assoc("dodatkowe",item.getDodatkowe()));
		else
			parm.add(new Assoc("dodatkowe", ""));
		

				
		Assoc[] assocData = new Assoc[parm.size()];
		assocData = parm.toArray(assocData);

		return render.useTemplate(this, "terminy/terminyForm", assocData);
	}
	
	/**
	 * Akcja nowego elementu
	 * @param app
	 * @param item
	 */
	private void NewItem(CoreApp app, TerminEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("New Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		// strona nie jest wyśietlana pierwszy raz, dane zostały przekazane przez formularz
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false; 
		}
		
		Template templ = new Template().setParentServlet(this);
		
		// trzeba wyświetlić formularz
		if(firstRender || !isOK[0]) {
			String preFrom = templ.useTemplate("form/formNewBegin", new Assoc[] {
					new Assoc("formTitle","Nowy termin"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formNewEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Dodaj nowy termin", new String[] {"przychodnia", "terminy", "Nowy termin"});
			
		} else {
			// nie ma błędów, można wykonać akcję
			new TerminyRep().setTermin(item).AddTermin();
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
			String msg = format1.format(item.getTermin());
			msg += " " + item.getPacjent().getImie() + " " + item.getPacjent().getNazwisko();
			msg += " " + (item.isZabieg()?"Zabieg":"Wizyta") + " " + item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko();
			
			app.Redirect(TABLE_PAGE + "?msg=new&text=" + msg );//+ item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("New Item end"));

	}
	private void EditItem(CoreApp app, TerminEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("EditItem"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
			
		Template templ = new Template().setParentServlet(this);
		
		if(firstRender || !isOK[0]) {

			String preFrom = templ.useTemplate("form/formEditBegin", new Assoc[] {
					new Assoc("formTitle","Edytuj termin"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formEditEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Edytuj termin", new String[] {"przychodnia", "terminy", "Edytuj termin"});
			
		} else {
						
			new TerminyRep().setTermin(item).EditTermin();
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
			String msg = format1.format(item.getTermin());
			msg += " " + item.getPacjent().getImie() + " " + item.getPacjent().getNazwisko();
			msg += " " + (item.isZabieg()?"Zabieg":"Wizyta") + " " + item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko();
			
			app.Redirect(TABLE_PAGE + "?msg=edit&text=" + msg);// + item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("EditItem end"));
	}
	
	private void ViewItem(CoreApp app, TerminEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("ViewItem"));
				
		if(item == null) {
			app.RedirectError();
		}
			
		Template templ = new Template().setParentServlet(this);
		
		String preFrom = templ.useTemplate("form/formViewBegin", new Assoc[] {
				new Assoc("formTitle","Podgląd ustalonego terminu"),
				new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
		});
		
		String postForm = templ.useTemplate("form/formViewEnd", new Assoc[] {});
		
		String form = this.RenderForm(templ,item);
					
		app.FinalRender(preFrom + form + postForm, "Podgląd terminu", new String[] {"przychodnia", "terminy", "Podgląd terminu"});
			
		logger.trace(TextTools.oneLine("ViewItem end"));
		
	}
	
	private void RemoveItem(CoreApp app, TerminEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("Remove Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
		
		Template templ = new Template().setParentServlet(this);
		
		
		if(firstRender) {

			String removeForm = templ.useTemplate("form/formRemove", new Assoc[] {
					new Assoc("formTitle","Usuń termin"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
					new Assoc("message","Czy napewno chcesz ususnąć termin <strong>" + /*item.getNazwa() +*/ "</strong>?"),
					new Assoc("warning", templ.useTemplate("terminy/removeWarning", new Assoc[]{}))
			});
						
			app.FinalRender(removeForm, "Usuń termin", new String[] {"przychodnia", "terminy", "Usuń termin"});
			
		} else {
			
			logger.trace(item.toString());
			
			Long id = item.getId();
			
			// dociągnięcie danych rodzaju wizyty
			ArrayList<TerminEnt> items = new TerminyRep().setId(id).getTerminy();
 			if(items.size() != 1)
 				app.RedirectError();
 			
 			item = items.get(0); // teraz mamy dane takie jak z id
			
			String msg = "";//item.getNazwa();
			
			new TerminyRep().setTermin(item).DeleteTermin(); // usunięcie 
			
			if(new TerminyRep().setId(id).getTerminy().size() == 0) // sprawdzenie czy się udało usunąć
				app.Redirect(TABLE_PAGE + "?msg=remove&text=" + msg);
			else
				app.Redirect(TABLE_PAGE + "?msg=fail&text=" + msg);
	
		}
		logger.trace(TextTools.oneLine("Remove end"));
	}
	
	private String todayDate() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");	        		
        return format1.format(cal.getTime()); 
	}

}
