package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.UzytkownicyRep;
import pl.edu.swsim.zw.entities.UzytkownikEnt;

/**
 * Servlet implementation class Uzytkownicy
 */
@WebServlet(
		description = "Lista użytkowników", 
		urlPatterns = { 
				"/uzytkownicy", 
				"/Uzytkownicy"
		})
public class Uzytkownicy extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Uzytkownicy() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
				
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("uzytkownicy"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("uzytkownik"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		UzytkownicyTableModel tableModel = new UzytkownicyTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
		
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		UzytkownicyRep repository = new UzytkownicyRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
        switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("login"));
        	break;
        case 1:
        	order.add(Order.desc("login"));
	        break;
        case 2: 
        	order.add(Order.asc("p.imie"));
        	order.add(Order.asc("p.nazwisko"));
        	break;
        case 3:
        	order.add(Order.desc("p.imie"));
        	order.add(Order.desc("p.nazwisko"));
	        break; 
        case 4: 
        	order.add(Order.asc("administrator"));
        	break;
        case 5:
        	order.add(Order.desc("administrator"));
	        break;

        }
        
        repository.setOrderBy(order);
        
        // szukany tekst jeśli jest zdefiniowany
        String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }
                
        tableModel.setModelData(repository.getUzytkownicy());
		
    	// Rednowanie tekstu tabeli	
		String newTable = superTable.RenderTable();
		
		
		// Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
		String successMsg = "";
		
		if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowego uzytkownika: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono uzytkownika: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto uzytkownika: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć uzytkownika: <strong>" + msgText + "</strong>";
				success = false;
			}
			else if(msgType.compareToIgnoreCase("admin")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie masz uprawnień do modyfikacji kont administratorów. <br />"
							+"Tę operację może wykonać tylko inny <strong>Administrator</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("base/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("base/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
		
		// rendowanie tabeli już z nagłówkiem i zawartością		
		newTable = new Template().setParentServlet(this).useTemplate("uzytkownicy/overviewTable", new Assoc[] {
				new Assoc("tableContent",newTable), 
				new Assoc("searchText",searchText), 
				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
				new Assoc("orderLink", superTable.getSortName()),
				new Assoc("message", successMsg),
		});
		
		// finałowe renderowanie strony
		app.FinalRender(newTable, "Lista użytkowników", new String[] {"dane", "uzytkownicy"});
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ta strona nie lubi post ;)
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "uzytkownicy"); 
	}

	/**
	 * Model danych tabeli
	 */
	public class UzytkownicyTableModel extends AbstractSuperTableModel<UzytkownikEnt> {
		 	

	    private final static int UZYTKOWNIK_IDX = 0;
	    private final static int PRACOWNIK_IDX = 1;
	    private final static int ADMINISTRATOR_IDX = 2;
	 
	    public UzytkownicyTableModel() {
			columnNames = new String[] {"Użytkownik", "Pracownik", "Administrator",}; // nazwy w Header
			dataNames = new String[] {"uzytkownik", "pracownik", "administrator",}; // nazwy dla sort
			// Z tych danych będzie utworzny select box do sortowania !
	    }
	    
	    // wyciąganie danych z listy
	    public String getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        UzytkownikEnt item = data.get(rowIndex);
	        switch (columnIndex) {
	            case UZYTKOWNIK_IDX:
	                return item.getLogin();
	            case PRACOWNIK_IDX:
	                return item.getPracownik().getImie() + " " + item.getPracownik().getNazwisko() 
	                		+ " (" + Long.toString(item.getPracownik().getId()) + ")";
	            case ADMINISTRATOR_IDX:
	                return (item.isAdministrator()?"Tak":"Nie");
	            default:
	                return "";
	        }
	    }
	    
	    // wyciaganie id rekodru po numerze w liście
		public String getRecordId(int row) {
			return Long.toString(data.get(row).getId());
		}
	    
		// reszta załatwia klasa tabeli
	}
}
