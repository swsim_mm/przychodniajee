package pl.edu.swsim.jee;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.dao.RozpoznaniaRep;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.RozpoznanieEnt;

/**
 * Servlet implementation class Rozpoznania
 */
@WebServlet(
		description = "Lista rozpoznań", 
		urlPatterns = { 
				"/rozpoznania", 
				"/Rozpoznania"
		})
public class Rozpoznania extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Rozpoznania() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("rozpoznania"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("rozpoznanie"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		RozpoznaniaTableModel tableModel = new RozpoznaniaTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
		
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		RozpoznaniaRep repository = new RozpoznaniaRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
        switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("w.termin"));
        	break;
        case 1: 
        	order.add(Order.desc("w.termin"));
        	break;
        case 2: 
        	order.add(Order.asc("choroba"));
        	break;
        case 3:
        	order.add(Order.desc("choroba"));
        	break;
        case 4: 
        	order.add(Order.asc("pr.imie"));
        	order.add(Order.asc("pr.nazwisko"));
        	break;
        case 5:
        	order.add(Order.desc("pr.imie"));
        	order.add(Order.desc("pr.nazwisko"));
	        break;  
        case 6: 
        	order.add(Order.asc("opis"));
        	break;
        case 7:
        	order.add(Order.desc("opis"));
	        break;  
        }
        
        repository.setOrderBy(order);
        
        // szukany tekst jeśli jest zdefiniowany
        /*String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }*/
        
        PacjentEnt pacjent = null;
        String pacjentText = "";
        Long pacjentId = 0L;
        
		if(request.getParameter("pacjent-id") != null && request.getParameter("pacjent-id").compareToIgnoreCase("0") != 0) {
			try {
				Long paId = Long.parseLong(request.getParameter("pacjent-id"));
				
				ArrayList<PacjentEnt> paList = new PacjenciRep().setId(paId).getPacjenci();
				if(paList.size() == 1) {
					pacjent = paList.get(0);
					pacjentText = pacjent.getImie() + " " + pacjent.getNazwisko();
					pacjentText += " - " + pacjent.getUlica().getNazwa() + " " + pacjent.getBudynek() + " " 
							+ pacjent.getMieszkanie() + ", " + pacjent.getUlica().getMiasto().getNazwa() 
							+ " " + pacjent.getUlica().getMiasto().getKod_pocztowy() + ", " 
							+ pacjent.getUlica().getMiasto().getWojewodztwo().getNazwa(); 
					pacjentId = pacjent.getId();
				}
				
			} catch(NumberFormatException e) { }
		}
		
		if(pacjent != null) {
			superTable.setExtraLink(superTable.getExtraLink() + "&pacjent-text=" + pacjentText + "&pacjent-id=" + pacjentId);
			repository.setPacjent(pacjent);
			tableModel.setModelData(repository.getRozpoznania());
		}
		
    	// Rednowanie tekstu tabeli	
		String newTable = superTable.RenderTable();
		
		// Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
		String successMsg = "";
		
		if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowe rozpoznanie: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono dane rozpoznania: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto rozpoznanie: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć rozpoznania: <strong>" + msgText + "</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
		
		
		// rendowanie tabeli już z nagłówkiem i zawartością		
		newTable = new Template().setParentServlet(this).useTemplate("rozpoznania/overviewTable", new Assoc[] {
				new Assoc("tableContent",newTable), 
				new Assoc("pacjent-text",pacjentText), 
				new Assoc("pacjent-id",Long.toString(pacjentId)),
				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
				new Assoc("orderLink", superTable.getSortName()),
				new Assoc("message", successMsg),
		});
		
		// finałowe renderowanie strony
		app.FinalRender(newTable, "Lista rozpoznań", new String[] {"przychodnia", "rozpoznania"});
	
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "rozpoznania"); 
	}
	
	/**
	 * Model danych tabeli
	 */
	public class RozpoznaniaTableModel extends AbstractSuperTableModel<RozpoznanieEnt> {
	 	

		private final static int TERMIN_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int CHOROBA_IDX = 1;
	    private final static int LEKARZ_IDX = 2;
	    private final static int OPIS_IDX = 3;

	 
	    public RozpoznaniaTableModel() {
			columnNames = new String[] {"Termin", "Rozpoznanie", "Lekarz", "Opis"}; // nazwy w Header
			dataNames = new String[] {"termin", "choroba", "lekarz", "opis"}; // nazwy dla sort
			// Z tych danych będzie utworzny select box do sortowania !
	    }
	    
	    // wyciąganie danych z listy
	    public String getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        RozpoznanieEnt item = data.get(rowIndex);
	        switch (columnIndex) {
	        case TERMIN_IDX:
	        	Calendar cal = Calendar.getInstance();
        		cal.setTime(item.getWizyta().getTermin());
        		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
                return format1.format(cal.getTime()); 	
                
	            case CHOROBA_IDX:
	                return (item.getChoroba().getNazwa());
	            case LEKARZ_IDX:
	                return ( item.getWizyta().getPracownik().getNazwisko() + " " + item.getWizyta().getPracownik().getImie());
	            case OPIS_IDX:
	                return item.getOpis();
	            default:
	                return "";
	        }
	    }
	    
	    // wyciaganie id rekodru po numerze w liście
		public String getRecordId(int row) {
			return Long.toString(data.get(row).getId());
		}
	    
		// reszta załatwia klasa tabeli
	}

}
