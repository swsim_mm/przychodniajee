package pl.edu.swsim.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;


/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);

		//PrintWriter out = response.getWriter(); // wyjście do odpowiedzi HTML
		
		if(app.isLogedIn()) {
			if(request.getParameter("go") != null && request.getParameter("go").compareToIgnoreCase("out") == 0) {
				app.Logout();
			}
		}
		
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "main"); 
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		//PrintWriter out = response.getWriter(); // wyjście do odpowiedzi HTML
				
		// użytkownik już zalogowany
		if(app.isLogedIn()) {
			response.setStatus(HttpServletResponse.SC_SEE_OTHER);
			response.setHeader("Location", ConfigData.appRootPath + "main"); 
		} else {
			// próba logowania z podanymi parametrami
			if (request.getParameter("user_name") != null 
					&& request.getParameter("user_password") != null
					&& app.Login(request.getParameter("user_name"), request.getParameter("user_password"))) {
				// udane logowanie
				response.setStatus(HttpServletResponse.SC_SEE_OTHER);
				response.setHeader("Location", ConfigData.appRootPath + "main"); 
				// bez owijania w bawełne przeniesienie na stronę główną 
				
			} else {
				String loginPage = new Template().useTemplate(this, "main/loginFailPage", new Assoc[] {});
				app.FinalRender(loginPage, "Błąd logowania", new String[] {"Logowanie", "Błąd logowania"});
			}
				
		}


	}

}
