package pl.edu.swsim.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;

/**
 * Servlet implementation class About
 */
@WebServlet(
		description = "Informacje o serwise", 
		urlPatterns = { 
				"/About", 
				"/about"
		})
public class About extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public About() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.trace(".");
		
		CoreApp app = new CoreApp(this, request, response);
		
		app.FinalRender(new Template().setParentServlet(this).useTemplate("main/aboutPage", new Assoc[] {}), "O Projekcie", new String[] {""});
		
	}

}
