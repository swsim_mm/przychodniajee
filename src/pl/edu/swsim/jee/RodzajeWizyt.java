package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.RodzajeWizytRep;
import pl.edu.swsim.zw.entities.RodzajWizytyEnt;

/**
 * Servlet implementation class RodzajeWizyt
 */
@WebServlet(
		description = "Lista rodzajów wizyt", 
		urlPatterns = { 
				"/rodzajewizyt", 
				"/RodzajeWizyt"
		})
public class RodzajeWizyt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RodzajeWizyt() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("rodzajewizyt"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("rodzajwizyty"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		RodzajeWizytTableModel tableModel = new RodzajeWizytTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
		
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		RodzajeWizytRep repository = new RodzajeWizytRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
        switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("nazwa"));
        	break;
        case 1:
        	order.add(Order.desc("nazwa"));
	        break;
        case 2: 
        	order.add(Order.asc("czastrwania"));
        	break;
        case 3:
        	order.add(Order.desc("czastrwania"));
	        break;  
        }
        
        repository.setOrderBy(order);
        
     // szukany tekst jeśli jest zdefiniowany
        String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }
        
        tableModel.setModelData(repository.getRodzajeWizyt());
        
     // Rendowanie tekstu tabeli	
     	String newTable = superTable.RenderTable();
     	
     // Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
		String successMsg = "";
		
		if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowy rodzaj wizyty: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono dane rodzaju wizyty: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto rodzaj wizyty: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć rodzaju wizyty: <strong>" + msgText + "</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
		
		// rendowanie tabeli już z nagłówkiem i zawartością		
		newTable = new Template().setParentServlet(this).useTemplate("rodzajewizyt/overviewTable", new Assoc[] {
				new Assoc("tableContent",newTable), 
				new Assoc("searchText",searchText), 
				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
				new Assoc("orderLink", superTable.getSortName()),
				new Assoc("message", successMsg),
		});
	
		// finałowe renderowanie strony
		app.FinalRender(newTable, "Lista rodzajów wizyt", new String[] {"slowniki", "rodzajewizyt"});

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "rodzajewizyt"); 
	}
	
	/**
	 * Model danych tabeli
	 */
	public class RodzajeWizytTableModel extends AbstractSuperTableModel<RodzajWizytyEnt> {
	 	

	    private final static int NAZWA_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int GABINET_IDX = 1;
	    private final static int CZAS_IDX = 2;

	 
	    public RodzajeWizytTableModel() {
			columnNames = new String[] {"Nazwa", "Gabinet", "Czas trwania"}; // nazwy w Header
			dataNames = new String[] {"nazwa", "gabinet", "czas"}; // nazwy dla sort
			// Z tych danych będzie utworzny select box do sortowania !
	    }
	    
	    // wyciąganie danych z listy
	    public String getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        RodzajWizytyEnt item = data.get(rowIndex);
	        switch (columnIndex) {
	            case NAZWA_IDX:
	                return item.getNazwa();
	            case GABINET_IDX:
	                return "(" + item.getGabinet().getNumer() + ") " + item.getGabinet().getNazwa();
	            case CZAS_IDX:
	            	Calendar cal = Calendar.getInstance();
	        		cal.setTime(item.getCzastrwania());
	            	return (Integer.toString(cal.get(Calendar.HOUR_OF_DAY))
	            			+ " godz. "
	            			+ Integer.toString(cal.get(Calendar.MINUTE))
	            			+ " min."
	            			);
	            default:
	                return "";
	        }
	    }
	    
	    // wyciaganie id rekodru po numerze w liście
		public String getRecordId(int row) {
			return Long.toString(data.get(row).getId());
		}
	    
		// reszta załatwia klasa tabeli
	}
	
}
