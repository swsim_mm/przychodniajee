package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.ChorobyRep;
import pl.edu.swsim.zw.entities.ChorobaEnt;

/**
 * Servlet implementation class Choroba
 */
@WebServlet(
		description = "dane oraz edycja choroby", 
		urlPatterns = { 
				"/choroba", 
				"/Choroba"
		})
public class Choroba extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
	// Wszystkie pola które mają być przechwycone z postData
		private static String[] formFields = new String[] {
			"id", "nazwa", "przewlekla", "opis",
		};
		// Strona do której mozna przekierować w razie małych 
		// błedów (danych wpowadzonych przez url)
		public static final String TABLE_PAGE = "choroby"; 
		public static final String FORM_PAGE = "choroba"; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Choroba() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
				if(!app.isLogedIn()) {
					app.RedirectLogin();
					return;
				}
				
				// gdzie iść przeniesnione w "mode"
				if(request.getParameter("mode") == null) {
					app.Redirect(TABLE_PAGE);
					return;
				} 
				
				String formMode = request.getParameter("mode");
				
				/******************************************************
				 * New  - nie trzeba niczego pobierać na starcie
				 */
				if(formMode.compareToIgnoreCase("new") == 0) {
					NewItem(app, new ChorobaEnt());
					return;
				}
				
				// dla reszty będzie trzeba pobrać id rekordu
				if(request.getParameter("id") == null) {
					app.Redirect(TABLE_PAGE);
					return;
				}
				String strId = request.getParameter("id");
				Long id;
				
				// Próba konwersji tekstu otrzymanego z url na liczbę long
				try {
					id = Long.parseLong(strId);
					if(id < 0)
						throw new Exception("Zły numer id");
				} catch(Exception e) {
					logger.trace(TextTools.twoLine("Błąd parsowania id"));
					app.Redirect(TABLE_PAGE); // taka pomyłka może wyniknać tylko z ingerencji adresem
					
					return;
				}
				/******************************************************
				 * Pobranie użytkownika	
				 */	
				ArrayList<ChorobaEnt> data = new ChorobyRep().setId(id).getChoroby();
				
				// ostatni raz opusczenie strony gdy coś będzie nie tak
				if(data.size() != 1) {
					logger.trace(TextTools.twoLine("Błąd pobierania rekordu po jego id"));
					app.RedirectError(); 
					return;
				} 
				
				ChorobaEnt item = data.get(0);
				
				// Mając użytkownika można wywołac pożądaną akcję
				if(formMode.compareToIgnoreCase("view") == 0) 
					ViewItem(app, item);
				else if(formMode.compareToIgnoreCase("edit") == 0) 
					EditItem(app, item); 
				else if(formMode.compareToIgnoreCase("remove") == 0) 
					RemoveItem(app, item);
				else 
					app.Redirect(TABLE_PAGE);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
			if(!app.isLogedIn()) {
				app.RedirectLogin();
				return;
			}
			
			// co będzie się działo zdefinowane w mode
			String formMode = request.getParameter("mode");
			
			// Mając użytkownika można wywołac pożądaną akcję
			// null wymusza pobranie danych z formatek
			if(formMode.compareToIgnoreCase("new") == 0)  {
				NewItem(app, null);
			} else if(formMode.compareToIgnoreCase("view") == 0)  {
				ViewItem(app, null);
			} else if(formMode.compareToIgnoreCase("edit") == 0) {
				EditItem(app, null);
			} else if(formMode.compareToIgnoreCase("remove") == 0) {
				RemoveItem(app, null);
			} else {
				app.Redirect(TABLE_PAGE);
			}
	}
	
	/**
	 * Pobiera dane z formatki i zapisuje je w rekordzie 
	 * @param request
	 * @param validResult
	 * @return
	 */
	private ChorobaEnt getDataResults(HttpServletRequest request, boolean[] validResult) {	
		logger.trace(".");
		boolean isOK = true; // gdy nie wychwycono znaczących błędów
		
		Map<String, String> postData = new HashMap<String, String>();
		
		// pobranie elementów formularza z request
		for(String field: formFields) {
			postData.put(field, request.getParameter(field));		
		}
		
		ChorobaEnt item = new ChorobaEnt();
		
		// id gdy edytowany jest istniejący 
		if(postData.get("id") != null) {
			logger.trace(TextTools.twoLine("ID (string):" + postData.get("id")));
			try { //trochę zachodu z konwersją
				Long id = Long.parseLong(postData.get("id"));
				if(id > 0) {
					item.setId(id); 
					logger.trace(TextTools.twoLine("ID:" + Long.toString(id)));
				}
			} catch (Exception e) { }
		}
		
		/*		"nazwa", "przewlekla", "opis"
		 * */
		
	
		if(postData.get("nazwa")==null || postData.get("nazwa").length() < 1)
			isOK = false;
		
		if(postData.get("przewlekla") != null && postData.get("przewlekla").compareToIgnoreCase("true")==0)
			item.setPrzewlekla(true);
		
		item.setNazwa(postData.get("nazwa"));
		item.setOpis(postData.get("opis"));
		

		validResult[0] = isOK; // java nie przekazuje referencji dla typów prymitywnych, stąd tablica
		return item;
	}
	
	/**
	 * Renderuje pola formatki
	 * @param render
	 * @param item
	 * @return
	 */
	private String RenderForm(Template render, ChorobaEnt item) {
		logger.trace(".");
		
		
		// zbieranie danych do formatki
		ArrayList<Assoc> parm = new ArrayList<Assoc>();
				
		parm.add(new Assoc("id",(Long.toString(item.getId()))));
		parm.add(new Assoc("nazwa",item.getNazwa()));
		parm.add(new Assoc("przewlekla",(item.isPrzewlekla()?"checked":"")));
		parm.add(new Assoc("opis",item.getOpis()));
				
		Assoc[] assocData = new Assoc[parm.size()];
		assocData = parm.toArray(assocData);

		return render.useTemplate(this, "choroby/chorobaForm", assocData);
	}
	
	/**
	 * Akcja nowego elementu
	 * @param app
	 * @param item
	 */
	private void NewItem(CoreApp app, ChorobaEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("New Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		// strona nie jest wyśietlana pierwszy raz, dane zostały przekazane przez formularz
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false; 
		}
		
		Template templ = new Template().setParentServlet(this);
		
		// trzeba wyświetlić formularz
		if(firstRender || !isOK[0]) {
			
			String preFrom = templ.useTemplate("form/formNewBegin", new Assoc[] {
					new Assoc("formTitle","Nowa choroba"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formNewEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Dodaj chorobę", new String[] {"slowniki", "choroby", "Nowa choroba"});
			
		} else {
			// nie ma błędów, można wykonać akcję
			new ChorobyRep().setChoroba(item).AddChoroba();
			app.Redirect(TABLE_PAGE + "?msg=new&text=" + item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("New Item end"));

	}
	
	private void EditItem(CoreApp app, ChorobaEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("EditItem"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
			
		Template templ = new Template().setParentServlet(this);
		
		if(firstRender || !isOK[0]) {

			String preFrom = templ.useTemplate("form/formEditBegin", new Assoc[] {
					new Assoc("formTitle","Edytuj chorobę"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formEditEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Edytuj chorobę", new String[] {"slowniki", "choroby", "Edytuj chorobę"});
			
		} else {
						
			new ChorobyRep().setChoroba(item).EditChoroba();
			//app.Redirect(TABLE_PAGE + "?msg=edit&text=" + item.getNazwa() + " " + item.isPrzewlekla() + " " + item.getOpis());
			app.Redirect(TABLE_PAGE + "?msg=edit&text=" + item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("EditItem end"));
	}
	
	private void ViewItem(CoreApp app, ChorobaEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("ViewItem"));
				
		if(item == null) {
			app.RedirectError();
		}
			
		Template templ = new Template().setParentServlet(this);
		
		String preFrom = templ.useTemplate("form/formViewBegin", new Assoc[] {
				new Assoc("formTitle","Podgląd choroby"),
				new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
		});
		
		String postForm = templ.useTemplate("form/formViewEnd", new Assoc[] {});
		
		String form = this.RenderForm(templ,item);
					
		app.FinalRender(preFrom + form + postForm, "Podgląd choroby", new String[] {"slowniki", "choroby", "Podgląd choroby"});
			

		logger.trace(TextTools.oneLine("ViewItem end"));
		
	}
	
	private void RemoveItem(CoreApp app, ChorobaEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("Remove Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
		
		Template templ = new Template().setParentServlet(this);
		
		
		if(firstRender) {

			String removeForm = templ.useTemplate("form/formRemove", new Assoc[] {
					new Assoc("formTitle","Usuń chorobę"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
					new Assoc("message","Czy napewno chcesz usunąć chorobę <strong>" + item.getNazwa() +  "</strong>?"),
					new Assoc("warning", templ.useTemplate("choroby/removeWarning", new Assoc[]{}))
			});
						
			app.FinalRender(removeForm, "Usuń chorobę", new String[] {"slowniki", "choroby", "Usuń chorobę"});
			
		} else {
			
			logger.trace(item.toString());
			
			Long id = item.getId();
			
			// dociągnięcie danych pracownika
			ArrayList<ChorobaEnt> items = new ChorobyRep().setId(id).getChoroby();
 			if(items.size() != 1)
 				app.RedirectError();
 			
 			item = items.get(0); // teraz mamy dane takie jak z id
			
			// String msg = item.getNazwa() + " " + (item.isPrzewlekla()?"Tak":"Nie") + " " + item.getOpis();
			String msg = item.getNazwa();
			
			new ChorobyRep().setChoroba(item).DeleteChoroba(); // usunięcie 
			
			if(new ChorobyRep().setId(id).getChoroby().size() == 0) // sprawdzenie czy się udało usunąć
				app.Redirect(TABLE_PAGE + "?msg=remove&text=" + msg);
			else
				app.Redirect(TABLE_PAGE + "?msg=fail&text=" + msg);
	
		}
		logger.trace(TextTools.oneLine("Remove end"));
	}
	
	

}
