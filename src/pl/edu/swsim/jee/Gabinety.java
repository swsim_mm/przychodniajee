package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.GabinetyRep;
import pl.edu.swsim.zw.entities.GabinetEnt;

/**
 * Servlet implementation class Gabinety
 */
@WebServlet(
		description = "Lista gabientów", 
		urlPatterns = { 
				"/gabinety", 
				"/Gabinety"
		})
public class Gabinety extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Gabinety() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
				
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("gabinety"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("gabinet"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		GabinetyTableModel tableModel = new GabinetyTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
		
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		GabinetyRep repository = new GabinetyRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
        switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("numer"));
        	break;
        case 1:
        	order.add(Order.desc("numer"));
	        break;
        case 2: 
        	order.add(Order.asc("nazwa"));
        	break;
        case 3:
        	order.add(Order.desc("nazwa"));
	        break;  
        }
        
        repository.setOrderBy(order);
        
        // szukany tekst jeśli jest zdefiniowany
        String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }
                
        tableModel.setModelData(repository.getGabinety());
		
    	// Rednowanie tekstu tabeli	
		String newTable = superTable.RenderTable();
		
		
		// Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
		String successMsg = "";
		
		if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowy gabinet: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono dane gabinetu: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto gabinet: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć gabinetu: <strong>" + msgText + "</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
		
		// rendowanie tabeli już z nagłówkiem i zawartością		
		newTable = new Template().setParentServlet(this).useTemplate("gabinety/overviewTable", new Assoc[] {
				new Assoc("tableContent",newTable), 
				new Assoc("searchText",searchText), 
				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
				new Assoc("orderLink", superTable.getSortName()),
				new Assoc("message", successMsg),
		});
		
		// finałowe renderowanie strony
		app.FinalRender(newTable, "Lista gabinetów", new String[] {"slowniki", "gabinety"});
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "gabienty"); 
		
	}
	
	/**
	 * Model danych tabeli
	 */
	public class GabinetyTableModel extends AbstractSuperTableModel<GabinetEnt> {
		 	
		//private ArrayList<PracownikEnt> data = null; // lista danych

	    private final static int NUMER_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int NAZWA_IDX = 1;

	 
	    public GabinetyTableModel() {
			columnNames = new String[] {"Numer", "Nazwa"}; // nazwy w Header
			dataNames = new String[] {"numer", "nazwa"}; // nazwy dla sort
			// Z tych danych będzie utworzny select box do sortowania !
	    }
	    
	    // wyciąganie danych z listy
	    public String getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        GabinetEnt item = data.get(rowIndex);
	        switch (columnIndex) {
	            case NUMER_IDX:
	                return item.getNumer();
	            case NAZWA_IDX:
	                return item.getNazwa();
	            default:
	                return "";
	        }
	    }
	    
	    // wyciaganie id rekodru po numerze w liście
		public String getRecordId(int row) {
			return Long.toString(data.get(row).getId());
		}
	    
		// reszta załatwia klasa tabeli
	}
}

