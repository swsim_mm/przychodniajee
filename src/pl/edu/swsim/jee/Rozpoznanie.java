package pl.edu.swsim.jee;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.ChorobyRep;
import pl.edu.swsim.zw.dao.RozpoznaniaRep;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.RozpoznanieEnt;
import pl.edu.swsim.zw.entities.ChorobaEnt;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Servlet implementation class Rozpoznanie
 */
@WebServlet(
		description = "dane oraz edycja rodzaju wizyty", 
		urlPatterns = { 
				"/rozpoznanie", 
				"/Rozpoznanie"
		})
public class Rozpoznanie extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
	// Wszystkie pola które mają być przechwycone z postData
			private static String[] formFields = new String[] {
				"id", "pacjent-text", "pacjent-id", 
				"choroba", "termin", "opis", 
			};
			// Strona do której mozna przekierować w razie małych 
			// błedów (danych wpowadzonych przez url)
			public static final String TABLE_PAGE = "rozpoznania"; 
			public static final String FORM_PAGE = "rozpoznanie";

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Rozpoznanie() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// gdzie iść przeniesnione w "mode"
		if(request.getParameter("mode") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		}
		
		String formMode = request.getParameter("mode");
		
		/******************************************************
		 * Dla new nie trzeba niczego pobierać na starcie
		 */
		if(formMode.compareToIgnoreCase("new") == 0) {
			NewItem(app, new RozpoznanieEnt());
			return;
		}
		
		// dla reszty będzie trzeba pobrać id rekordu
		if(request.getParameter("id") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 
		String strId = request.getParameter("id");
		Long id;
		
		// Próba konwersji tekstu otrzymanego z url na liczbę long
		try {
			id = Long.parseLong(strId);
			if(id < 0)
				throw new Exception("Zły numer id");
		} catch(Exception e) {
			logger.trace(TextTools.twoLine("Błąd parsowania id"));
			app.Redirect(TABLE_PAGE); // taka pomyłka może wyniknać tylko z ingerencji adresem
			
			return;
		}
			
		/******************************************************
		 * Pobranie użytkownika	
		 */	
		ArrayList<RozpoznanieEnt> data = new RozpoznaniaRep().setId(id).getRozpoznania();
		
		// ostatni raz opusczenie strony gdy coś będzie nie tak
		if(data.size() != 1) {
			logger.trace(TextTools.twoLine("Błąd pobierania rekordu po jego id"));
			app.RedirectError(); 
			return;
		} 
		
		RozpoznanieEnt item = data.get(0);
		
		// Mając użytkownika można wywołac pożądaną akcję
		if(formMode.compareToIgnoreCase("view") == 0) 
			ViewItem(app, item);
		else if(formMode.compareToIgnoreCase("edit") == 0) 
			EditItem(app, item); 
		else if(formMode.compareToIgnoreCase("remove") == 0) 
			RemoveItem(app, item);
		else 
			app.Redirect(TABLE_PAGE);
		
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// co będzie się działo zdefinowane w mode
		String formMode = request.getParameter("mode");
		
		// Mając użytkownika można wywołac pożądaną akcję
		// null wymusza pobranie danych z formatek
		if(formMode.compareToIgnoreCase("new") == 0)  {
			NewItem(app, null);
		} else if(formMode.compareToIgnoreCase("view") == 0)  {
			ViewItem(app, null);
		} else if(formMode.compareToIgnoreCase("edit") == 0) {
			EditItem(app, null);
		} else if(formMode.compareToIgnoreCase("remove") == 0) {
			RemoveItem(app, null);
		} else {
			app.Redirect(TABLE_PAGE);
		}
		
	}
	
	/**
	 * Pobiera dane z formatki i zapisuje je w rekordzie 
	 * @param request
	 * @param validResult
	 * @return
	 */
	private RozpoznanieEnt getDataResults(HttpServletRequest request, boolean[] validResult) {	
		logger.trace(".");
		boolean isOK = true; // gdy nie wychwycono znaczących błędów
		
		Map<String, String> postData = new HashMap<String, String>();
		
		// pobranie elementów formularza z request
		for(String field: formFields) {
			postData.put(field, request.getParameter(field));		
		}
		
		RozpoznanieEnt item = new RozpoznanieEnt();
		
		// id gdy edytowany jest istniejący 
		if(postData.get("id") != null) {
			logger.trace(TextTools.twoLine("ID (string):" + postData.get("id")));
			try { //trochę zachodu z konwersją
				Long id = Long.parseLong(postData.get("id"));
				if(id > 0) {
					item.setId(id); 
					logger.trace(TextTools.twoLine("ID:" + Long.toString(id)));
				}
			} catch (Exception e) { }
		}
		
		@SuppressWarnings("unused")
		PacjentEnt pacjent = null;
		
		if(postData.get("pacjent-id") != null && postData.get("pacjent-id").compareToIgnoreCase("0") != 0) {
			try {
				Long prId = Long.parseLong(postData.get("pacjent-id"));
				
				ArrayList<PacjentEnt> prList = new PacjenciRep().setId(prId).getPacjenci();
				if(prList.size() == 1)
					pacjent = prList.get(0);
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
		
		if(postData.get("choroba") != null && postData.get("choroba").compareToIgnoreCase("0") != 0) {
			try {
				Long chid = Long.parseLong(postData.get("choroba"));
				
				ArrayList<ChorobaEnt> chList = new ChorobyRep().setId(chid).getChoroby();
				if(chList.size() == 1)
					item.setChoroba(chList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
		
		if(postData.get("termin") != null && postData.get("termin").compareToIgnoreCase("0") != 0) {
			try {
				Long terid = Long.parseLong(postData.get("termin"));
				
				ArrayList<TerminEnt> terList = new TerminyRep().setId(terid).getTerminy();
				if(terList.size() == 1)
					item.setWizyta(terList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
		
		if(postData.get("opis") != null) {
			item.setOpis(postData.get("opis"));
		}
		
						
		validResult[0] = isOK; // java nie przekazuje referencji dla typów prymitywnych, stąd tablica
		return item;
	}
	
	/**
	 * Renderuje pola formatki
	 * @param render
	 * @param item
	 * @return
	 */
	private String RenderForm(Template render, RozpoznanieEnt item) {
		logger.trace(".");
		
		
		// zbieranie danych do formatki
		ArrayList<Assoc> parm = new ArrayList<Assoc>();
				
		parm.add(new Assoc("id",(Long.toString(item.getId()))));
	
		parm.add(new Assoc("opis", item.getOpis()));
		

		
		// rozpoznana choroba
		Long idChoroba = -1L;
		if(item.getChoroba() != null)
			idChoroba = item.getChoroba().getId();
		
		// generowanie select-option z wybrną chorobą
		String choroby = "";
		ArrayList<ChorobaEnt> chorobyList = new ChorobyRep().getChoroby();
		for(int i = 0; i<chorobyList.size(); i++) 
		{
			choroby += "<option value=\"" + chorobyList.get(i).getId() + "\"";
			if(item.getChoroba() != null && chorobyList.get(i).getId() == idChoroba)
				choroby += " selected";
			choroby += ">";
			choroby += chorobyList.get(i).getNazwa();
			choroby += "</option>";
		}		
		parm.add(new Assoc("choroba",choroby));
		
		// 
		Long pacjentId = -1L;
		String pacjentText = "";
		
		if(item.getWizyta() != null && item.getWizyta().getPacjent() != null) {
			pacjentId = item.getWizyta().getPacjent().getId();
		
			pacjentText = item.getWizyta().getPacjent().getImie() + " " + item.getWizyta().getPacjent().getNazwisko();
			pacjentText += " - " + item.getWizyta().getPacjent().getUlica().getNazwa() + " " + item.getWizyta().getPacjent().getBudynek() + " " 
				+ item.getWizyta().getPacjent().getMieszkanie() + ", " + item.getWizyta().getPacjent().getUlica().getMiasto().getNazwa() 
				+ " " + item.getWizyta().getPacjent().getUlica().getMiasto().getKod_pocztowy() + ", " 
				+ item.getWizyta().getPacjent().getUlica().getMiasto().getWojewodztwo().getNazwa(); 
		}
		
		parm.add(new Assoc("pacjent-id", Long.toString(pacjentId)));
		parm.add(new Assoc("pacjent-text", pacjentText));
		
		String terminyLista = "";
		if(pacjentId > 0) {
			TerminyRep repository = new TerminyRep();
		
			
			repository.setPacjent(item.getWizyta().getPacjent());
			
			ArrayList<Order> orderList = new ArrayList<Order>();
			orderList.add(Order.asc("termin"));
			repository.setOrderBy(orderList);
			
			ArrayList<TerminEnt> terminy = repository.getTerminy();
			
			for (int i = 0; i < terminy.size(); i++) {
				terminyLista += "<option value=\"";
				terminyLista += Long.toString(terminy.get(i).getId()) + "\"";
				if(item.getWizyta()!=null && item.getWizyta().getId() == terminy.get(i).getId())
					terminyLista += " selected";
				terminyLista += ">";
				
	    		SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd HH:mm");	        		
				String termin = format1.format(terminy.get(i).getTermin());
				
				terminyLista += termin;
				terminyLista += (terminy.get(i).isZabieg()?" Zabieg":" Wizyta");
				terminyLista += "</option>";
			}
			
		}
		
		parm.add(new Assoc("termin", terminyLista));
		
		//
				
		Assoc[] assocData = new Assoc[parm.size()];
		assocData = parm.toArray(assocData);

		return render.useTemplate(this, "rozpoznania/rozpoznanieForm", assocData);
	}
	
	/**
	 * Akcja nowego elementu
	 * @param app
	 * @param item
	 */
	private void NewItem(CoreApp app, RozpoznanieEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("New Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		// strona nie jest wyśietlana pierwszy raz, dane zostały przekazane przez formularz
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false; 
		}
		
		Template templ = new Template().setParentServlet(this);
		
		// trzeba wyświetlić formularz
		if(firstRender || !isOK[0]) {
			
			String preFrom = templ.useTemplate("form/formNewBegin", new Assoc[] {
					new Assoc("formTitle","Nowe rozpoznanie"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formNewEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Dodaj rodzaj wizyty", new String[] {"przychodnia", "rozpoznania", "Nowe rozpoznanie"});
			
		} else {
			// nie ma błędów, można wykonać akcję
			new RozpoznaniaRep().setRozpoznanie(item).AddRozpoznanie();
			String msg = item.getWizyta().getPacjent().getImie() + " " + item.getWizyta().getPacjent().getNazwisko();
			msg += " " + item.getChoroba().getNazwa();
			app.Redirect(TABLE_PAGE + "?msg=new&text=" + msg 
					+ "&pacjent-text=" + item.getWizyta().getPacjent().getImie() + " " 
					+ item.getWizyta().getPacjent().getNazwisko() + "&pacjent-id=" + item.getWizyta().getPacjent().getId());//+ item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("New Item end"));

	}
	private void EditItem(CoreApp app, RozpoznanieEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("EditItem"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
			
		Template templ = new Template().setParentServlet(this);
		
		if(firstRender || !isOK[0]) {

			String preFrom = templ.useTemplate("form/formEditBegin", new Assoc[] {
					new Assoc("formTitle","Edytuj rozpoznanie"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formEditEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Edytuj rozpoznanie", new String[] {"przychodnia", "rozpoznania", "Edytuj rozpoznanie"});
			
		} else {
						
			new RozpoznaniaRep().setRozpoznanie(item).EditRozpoznanie();
			
			String msg = item.getWizyta().getPacjent().getImie() + " " + item.getWizyta().getPacjent().getNazwisko();
			msg += " " + item.getChoroba().getNazwa();
			
			app.Redirect(TABLE_PAGE + "?msg=edit&text=" + msg 
					+ "&pacjent-text=" + item.getWizyta().getPacjent().getImie() + " " 
					+ item.getWizyta().getPacjent().getNazwisko() + "&pacjent-id=" + item.getWizyta().getPacjent().getId());
			
		}
		logger.trace(TextTools.oneLine("EditItem end"));
	}
	private void ViewItem(CoreApp app, RozpoznanieEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("ViewItem"));
				
		if(item == null) {
			app.RedirectError();
		}
			
		Template templ = new Template().setParentServlet(this);
		
		String preFrom = templ.useTemplate("form/formViewBegin", new Assoc[] {
				new Assoc("formTitle","Podgląd rozpoznania"),
				new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
		});
		
		String postForm = templ.useTemplate("form/formViewEnd", new Assoc[] {});
		
		String form = this.RenderForm(templ,item);
					
		app.FinalRender(preFrom + form + postForm, "Podgląd rozpoznania", new String[] {"przychodnia", "rozpoznania", "Podgląd rozpoznania"});
			
		logger.trace(TextTools.oneLine("ViewItem end"));
		
	}
	private void RemoveItem(CoreApp app, RozpoznanieEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("Remove Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
		
		Template templ = new Template().setParentServlet(this);
		
		
		if(firstRender) {

			String removeForm = templ.useTemplate("form/formRemove", new Assoc[] {
					new Assoc("formTitle","Usuń rozpoznanie"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
					new Assoc("message","Czy napewno chcesz ususnąć rozpoznanie <strong>" + /* item.getNazwa() +*/ "</strong>?"),
					new Assoc("warning", templ.useTemplate("rozpoznania/removeWarning", new Assoc[]{}))
			});
						
			app.FinalRender(removeForm, "Usuń rozpoznanie", new String[] {"przychodnia", "rozpoznania", "Usuń rozpoznanie"});
			
		} else {
			
			logger.trace(item.toString());
			
			Long id = item.getId();
			
			// dociągnięcie danych rodzaju wizyty
			ArrayList<RozpoznanieEnt> items = new RozpoznaniaRep().setId(id).getRozpoznania();
 			if(items.size() != 1)
 				app.RedirectError();
 			
 			item = items.get(0); // teraz mamy dane takie jak z id
			
			String msg = ""; //item.getNazwa();
			
			new RozpoznaniaRep().setRozpoznanie(item).DeleteRozpoznanie(); // usunięcie 
			
			if(new RozpoznaniaRep().setId(id).getRozpoznania().size() == 0) // sprawdzenie czy się udało usunąć
				app.Redirect(TABLE_PAGE + "?msg=remove&text=" + msg);
			else
				app.Redirect(TABLE_PAGE + "?msg=fail&text=" + msg);
	
		}
		logger.trace(TextTools.oneLine("Remove end"));
	}
	
	

}
