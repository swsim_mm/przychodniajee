package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.LokalizacjeRep;
import pl.edu.swsim.zw.dao.PacjenciRep;
import pl.edu.swsim.zw.dao.PracownicyRep;
import pl.edu.swsim.zw.entities.MiastoEnt;
import pl.edu.swsim.zw.entities.PacjentEnt;
import pl.edu.swsim.zw.entities.PracownikEnt;
import pl.edu.swsim.zw.entities.UlicaEnt;
import pl.edu.swsim.zw.entities.WojewodztwoEnt;

/**
 * Servlet implementation class Pacjent
 */
@WebServlet(
		description = "Edycja pacjenta", 
		urlPatterns = { 
				"/pacjent", 
				"/Pacjent"
		})
public class Pacjent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
	// Wszystkie pola które mają być przechwycone z postData
	private static String[] formFields = new String[] {
		"id", "imie", "nazwisko", "pesel", "nfz", "lekarz", 
		"wojewodztwo", "miasto-text", "miasto-id", "ulica-text", "ulica-id", 
		"budynek", "mieszkanie", "telefon", "dodatkowe"
	};
	// Strona do której mozna przekierować w razie małych 
	// błedów (danych wpowadzonych przez url)
	public static final String TABLE_PAGE = "pacjenci"; 
	public static final String FORM_PAGE = "pacjent"; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pacjent() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// gdzie iść przeniesnione w "mode"
		if(request.getParameter("mode") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 

		String formMode = request.getParameter("mode");
		
		/******************************************************
		 * Dla new nie trzeba niczego pobierać na starcie
		 */
		if(formMode.compareToIgnoreCase("new") == 0) {
			NewItem(app, new PacjentEnt());
			return;
		}
		
		// dla reszty będzie trzeba pobrać id rekordu
		if(request.getParameter("id") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 
		String strId = request.getParameter("id");
		Long id;
					
		// Próba konwersji tekstu otrzymanego z url na liczbę long
		try {
			id = Long.parseLong(strId);
			if(id < 0)
				throw new Exception("Zły numer id");
		} catch(Exception e) {
			logger.trace(TextTools.twoLine("Błąd parsowania id"));
			app.Redirect(TABLE_PAGE); // taka pomyłka może wyniknać tylko z ingerencji adresem
			
			return;
		}
		
		/******************************************************
		 * Pobranie użytkownika	
		 */	
		ArrayList<PacjentEnt> data = new PacjenciRep().setId(id).getPacjenci();
		
		// ostatni raz opusczenie strony gdy coś będzie nie tak
		if(data.size() != 1) {
			logger.trace(TextTools.twoLine("Błąd pobierania rekordu po jego id"));
			app.RedirectError(); 
			return;
		} 
		
		PacjentEnt item = data.get(0);
		
		// Mając użytkownika można wywołac pożądaną akcję
		if(formMode.compareToIgnoreCase("view") == 0) 
			ViewItem(app, item);
		else if(formMode.compareToIgnoreCase("edit") == 0) 
			EditItem(app, item); 
		else if(formMode.compareToIgnoreCase("remove") == 0) 
			RemoveItem(app, item);
		else 
			app.Redirect(TABLE_PAGE);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// co będzie się działo zdefinowane w mode
		String formMode = request.getParameter("mode");
		
		// Mając użytkownika można wywołac pożądaną akcję
		// null wymusza pobranie danych z formatek
		if(formMode.compareToIgnoreCase("new") == 0)  {
			NewItem(app, null);
		} else if(formMode.compareToIgnoreCase("view") == 0)  {
			ViewItem(app, null);
		} else if(formMode.compareToIgnoreCase("edit") == 0) {
			EditItem(app, null);
		} else if(formMode.compareToIgnoreCase("remove") == 0) {
			RemoveItem(app, null);
		} else {
			app.Redirect(TABLE_PAGE);
		}
			
	}
	
	/**
	 * Pobiera dane z formatki i zapisuje je w rekordzie 
	 * @param request
	 * @param validResult
	 * @return
	 */
	private PacjentEnt getDataResults(HttpServletRequest request, boolean[] validResult) {	
		logger.trace(".");
		boolean isOK = true; // gdy nie wychwycono znaczących błędów
		
		Map<String, String> postData = new HashMap<String, String>();
		
		// pobranie elementów formularza z request
		for(String field: formFields) {
			postData.put(field, request.getParameter(field));		
		}
		
		PacjentEnt item = new PacjentEnt();
		
		// id gdy edytowany jest istniejący użytkownik
		if(postData.get("id") != null) {
			logger.trace(TextTools.twoLine("ID (string):" + postData.get("id")));
			try { //trochę zachodu z konwersją
				Long id = Long.parseLong(postData.get("id"));
				if(id > 0) {
					item.setId(id); 
					logger.trace(TextTools.twoLine("ID:" + Long.toString(id)));
				}
			} catch (Exception e) { }
		}
		
		/*		"imie", "nazwisko", "pesel", "nfz", "lekarz", 
		"wojewodztwo", "miasto-text", "miasto-id", "ulica-text", "ulica-id", 
		"budynek", "mieszkanie", "telefon", "dodatkowe"
		 * */
		
		item.setImie(postData.get("imie"));
		item.setNazwisko(postData.get("nazwisko"));
		item.setPesel(postData.get("pesel"));
		item.setKarta_nfz(postData.get("nfz"));
		
		item.setBudynek(postData.get("budynek"));
		item.setMieszkanie(postData.get("mieszkanie"));
		item.setTelefon(postData.get("telefon"));
		item.setDodatkowe(postData.get("dodatkowe"));
		

		// Trochę zachodu z adresem
		// Umożliwia użytkownikowi przypisanie adresu który mozę jeszcze nie istnieć
		
		Long wojId = 0L, miastoId = 0L, ulicaId = 0L;
		
		// zebranie numerów id z pól
		try {
			wojId = Long.parseLong(postData.get("wojewodztwo"));
		} catch(NumberFormatException e) {
			isOK = false;
		}
		try {
			miastoId = Long.parseLong(postData.get("miasto-id"));
		} catch(NumberFormatException e) {
			isOK = false;
		}
		try {
			ulicaId = Long.parseLong(postData.get("ulica-id"));
		} catch(NumberFormatException e) {
			isOK = false;
		}
		
		WojewodztwoEnt wojewodztwo = new WojewodztwoEnt();
		MiastoEnt miasto = new MiastoEnt();
		UlicaEnt ulica = new UlicaEnt();
		
		// ulica ma id więc jest to wartość wybrana z bazy istniejącej ulicy 
		if(ulicaId > 0) {
			
			ArrayList<UlicaEnt> ulice = new LokalizacjeRep().setId(ulicaId).getUlice();
			if(ulice.size()==1) {
				ulica = ulice.get(0);
				logger.trace("Znane ulica");
			} else // gdy nie da się pobrać to coś nie tak zostało ustawione 
				isOK = false;
			
			item.setUlica(ulica);
			
		} else if(ulicaId == 0 && miastoId > 0) { // ulica wpisana a miasto wybrane z pola
			
			ArrayList<MiastoEnt> miasta = new LokalizacjeRep().setId(miastoId).getMiasta();
			
			if(miasta.size()==1) {
				miasto = miasta.get(0);	
				logger.trace("Znane miasto");
			} else
				isOK = false;
			
			ulica.setNazwa(postData.get("ulica-text"));
			
			ulica.setMiasto(miasto);
			item.setUlica(ulica);
			
		} else /*if(ulicaId == 0 && miastoId == 0)*/ { // ulica i miasto wpisane z palca
			
			ArrayList<WojewodztwoEnt> wojeowdztwa = new LokalizacjeRep().setId(wojId).getWojewodztwa();		
			
			if(wojeowdztwa.size() == 1)
				wojewodztwo = wojeowdztwa.get(0);
			else
				isOK = false;
			
			String miastoTest = postData.get("miasto-text");
			
			if(miastoTest != null) {
				
				String kodPattern = "(.*)([0-9]{2}[-][0-9]{3})";
				Pattern pattern = Pattern.compile(kodPattern);
				Matcher matcher = pattern.matcher(miastoTest); 
				
				if(matcher.matches() && matcher.groupCount() == 2) {
					miasto.setNazwa(matcher.group(1));
					miasto.setKod_pocztowy(matcher.group(2));
				} else {
					miasto.setNazwa(miastoTest);
					miasto.setKod_pocztowy("");
				}
			}
			
			ulica.setNazwa(postData.get("ulica-text"));
			
			
			miasto.setWojewodztwo(wojewodztwo);
			ulica.setMiasto(miasto);
			item.setUlica(ulica);
		}
		
		
		if(postData.get("lekarz") != null && postData.get("lekarz").length() > 0 && postData.get("lekarz").compareToIgnoreCase("0") != 0) {
			try {
				Long prId = Long.parseLong(postData.get("lekarz"));
				
				ArrayList<PracownikEnt> prList = new PracownicyRep().setId(prId).getPracownicy();
				if(prList.size() == 1)
					item.setPracownik(prList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
				
		validResult[0] = isOK; // java nie przekazuje referencji dla typów prymitywnych, stąd tablica
		return item;
	}
	
	/**
	 * Renderuje pola formatki
	 * @param render
	 * @param item
	 * @return
	 */
	private String RenderForm(Template render, PacjentEnt item) {
		logger.trace(".");
		
		// Adres może nie mieć wybrane województwa, lepiej sie upewnić
		Long idWojewodztwo = -1L;
		if(item.getUlica() != null && item.getUlica().getMiasto() !=null && item.getUlica().getMiasto().getWojewodztwo() != null)
			idWojewodztwo = item.getUlica().getMiasto().getWojewodztwo().getId();
		
		// generowanie select-option z wybraym przypisanym wojewodztwem
		String wojewodztwa = "";
		ArrayList<WojewodztwoEnt> woj = new LokalizacjeRep().getWojewodztwa();
		for(int i = 0; i<woj.size(); i++) 
		{
			wojewodztwa += "<option value=\"" + woj.get(i).getId() + "\"";
			if(woj.get(i).getId() == idWojewodztwo)
				wojewodztwa += " selected";
			wojewodztwa += ">";
			wojewodztwa += woj.get(i).getNazwa();
			wojewodztwa += "</option>";
		}
		
		// Lekarz rodzinny
		Long idLekarz = -1L;
		if(item.getPracownik() != null)
			idLekarz = item.getPracownik().getId();
		
		// generowanie select-option z wybraym przypisanym lekarzem
		String lekarze = "";
		ArrayList<PracownikEnt> lekarzeList = new PracownicyRep().setRodzajPracownika(PracownikEnt.TYP_LEKARZ).getPracownicy();
		for(int i = 0; i<lekarzeList.size(); i++) 
		{
			lekarze += "<option value=\"" + lekarzeList.get(i).getId() + "\"";
			if(lekarzeList.get(i).getId() == idLekarz)
				lekarze += " selected";
			lekarze += ">";
			lekarze += lekarzeList.get(i).getImie() + " " + lekarzeList.get(i).getNazwisko();
			lekarze += "</option>";
		}
		
		
		// zbieranie danych do formatki
		ArrayList<Assoc> parm = new ArrayList<Assoc>();
				
		parm.add(new Assoc("id",(Long.toString(item.getId()))));
		parm.add(new Assoc("imie",(item.getImie())));
		parm.add(new Assoc("nazwisko",item.getNazwisko()));
		parm.add(new Assoc("pesel",item.getPesel()));
		parm.add(new Assoc("nfz",item.getKarta_nfz()));
		parm.add(new Assoc("lekarz",lekarze));
		parm.add(new Assoc("telefon",item.getTelefon()));
		parm.add(new Assoc("dodatkowe", item.getDodatkowe()));
		parm.add(new Assoc("wojewodztwa",wojewodztwa));

		if(item.getUlica() != null && item.getUlica().getMiasto() != null) {
			parm.add(new Assoc("miastoId",Long.toString(item.getUlica().getMiasto().getId())));
			parm.add(new Assoc("miastoText",item.getUlica().getMiasto().getNazwa() + " " + item.getUlica().getMiasto().getKod_pocztowy() ));
			parm.add(new Assoc("ulicaId",Long.toString(item.getUlica().getId())));
			parm.add(new Assoc("ulicaText",item.getUlica().getNazwa()));
		} else {
			parm.add(new Assoc("miastoId","0"));
			parm.add(new Assoc("miastoText","" ));
			parm.add(new Assoc("ulicaId","0"));
			parm.add(new Assoc("ulicaText",""));
		}
		
		parm.add(new Assoc("budynek",item.getBudynek()));
		parm.add(new Assoc("mieszkanie",item.getMieszkanie()));
				
		Assoc[] assocData = new Assoc[parm.size()];
		assocData = parm.toArray(assocData);

		return render.useTemplate(this, "pacjenci/pacjentForm", assocData);
	}
	
	/**
	 * Akcja nowego elementu
	 * @param app
	 * @param item
	 */
	private void NewItem(CoreApp app, PacjentEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("New Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		// strona nie jest wyśietlana pierwszy raz, dane zostały przekazane przez formularz
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false; 
		}
		
		Template templ = new Template().setParentServlet(this);
		
		// trzeba wyświetlić formularz
		if(firstRender || !isOK[0]) {
			
			String preFrom = templ.useTemplate("form/formNewBegin", new Assoc[] {
					new Assoc("formTitle","Nowy pacjent"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formNewEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Dodaj pacjenta", new String[] {"dane", "pacjenci", "Nowy pacjent"});
			
		} else {
			// nie ma błędów, można wykonać akcję
			new PacjenciRep().setPacjent(item).AddPacjent();
			app.Redirect(TABLE_PAGE + "?msg=new&text=" + item.getImie() + " " + item.getNazwisko());
			
		}
		logger.trace(TextTools.oneLine("New Item end"));

	}
	
	private void EditItem(CoreApp app, PacjentEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("EditItem"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
			
		Template templ = new Template().setParentServlet(this);
		
		if(firstRender || !isOK[0]) {

			String preFrom = templ.useTemplate("form/formEditBegin", new Assoc[] {
					new Assoc("formTitle","Edytuj pacjenta"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formEditEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Edytuj pacjenta", new String[] {"dane", "pacjenci", "Edytuj pacjenta"});
			
		} else {
						
			new PacjenciRep().setPacjent(item).EditPacjent();
			app.Redirect(TABLE_PAGE + "?msg=edit&text=" + item.getImie() + " " + item.getNazwisko());
			
		}
		logger.trace(TextTools.oneLine("EditItem end"));
	}
	

	
	private void ViewItem(CoreApp app, PacjentEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("ViewItem"));
				
		if(item == null) {
			app.RedirectError();
		}
			
		Template templ = new Template().setParentServlet(this);
		
		String preFrom = templ.useTemplate("form/formViewBegin", new Assoc[] {
				new Assoc("formTitle","Podgląd pacjenta"),
				new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
		});
		
		String postForm = templ.useTemplate("form/formViewEnd", new Assoc[] {});
		
		String form = this.RenderForm(templ,item);
					
		app.FinalRender(preFrom + form + postForm, "Podgląd pacjenta", new String[] {"dane", "pacjenci", "Podgląd pacjenta"});
			

		logger.trace(TextTools.oneLine("ViewItem end"));
		
	}
	
	private void RemoveItem(CoreApp app, PacjentEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("Remove Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
		
		Template templ = new Template().setParentServlet(this);
		
		
		if(firstRender) {

			String removeForm = templ.useTemplate("form/formRemove", new Assoc[] {
					new Assoc("formTitle","Usuń pacjenta"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
					new Assoc("message","Czy napewno chcesz ususnąć pacjenta <strong>" + item.getImie() + " " + item.getNazwisko() + "</strong>?"),
					new Assoc("warning", templ.useTemplate("pacjenci/removeWarning", new Assoc[]{}))
			});
						
			app.FinalRender(removeForm, "Usuń pracownika", new String[] {"dane", "pacjenci", "Nowy pacjent"});
			
		} else {
			
			logger.trace(item.toString());
			
			Long id = item.getId();
			
			// dociągnięcie danych pracownika
			ArrayList<PacjentEnt> items = new PacjenciRep().setId(id).getPacjenci();
 			if(items.size() != 1)
 				app.RedirectError();
 			
 			item = items.get(0); // teraz mamy dane takie jak imie i nazwisko
			
			String msg = item.getImie() + " " + item.getNazwisko();
			
			new PacjenciRep().setPacjent(item).DeletePacjent(); // usunięcie 
			
			if(new PacjenciRep().setId(id).getPacjenci().size() == 0) // sprawdzenie czy się udało usunąć
				app.Redirect(TABLE_PAGE + "?msg=remove&text=" + msg);
			else
				app.Redirect(TABLE_PAGE + "?msg=fail&text=" + msg);
	
		}
		logger.trace(TextTools.oneLine("Remove end"));
	}

}
