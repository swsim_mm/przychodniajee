package pl.edu.swsim.jee;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.criterion.Order;

import pl.edu.swsim.jee.core.AbstractSuperTableModel;
import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.ConfigData;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.SuperTable;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.dao.TerminyRep;
import pl.edu.swsim.zw.entities.TerminEnt;

/**
 * Servlet implementation class Terminy
 */
@WebServlet(
		description = "Lista ustalonych terminów", 
		urlPatterns = { 
				"/terminy", 
				"/Terminy"
		})
public class Terminy extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Terminy() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie użytkowników niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// Super klasa tabeli ;)
		SuperTable superTable = new SuperTable();
		superTable.setSortTargetLink("terminy"); // link do sortowania (czyli strona na której się znajduje)
		superTable.setRecordTargetLink("termin"); // link do oglądania rekordu
		superTable.setTemplate(new Template().setParentServlet(this)); // obiekt template do skórek, z przypisanym servletem
		
		TerminyTableModel tableModel = new TerminyTableModel(); // model z danymi zdefiniowany na końcu w klasie
		superTable.setModel(tableModel); 
		
		// przetłumaczona na liczbę opcja sortowania
		int orderNumber = 0; // domyślna
		
		if(request.getParameter("order") != null) // gdy nie została podana w adresie
			orderNumber = superTable.setSortTag(request.getParameter("order"));
		
		TerminyRep repository = new TerminyRep(); // repozytorium
		ArrayList<Order> order = new ArrayList<Order>(); // order dla wybrania z repozytorium
		
		// ustawienia sortowania według
        switch(orderNumber) { 
        case 0: 
        	order.add(Order.asc("termin"));
        	break;
        case 1:
        	order.add(Order.desc("termin"));
	        break;
        case 2: 
        	order.add(Order.asc("zabieg"));
        	break;
        case 3:
        	order.add(Order.desc("zabieg"));
	        break; 
        case 4: 
        	;
        	break;
        case 5:
        	;
	        break;
        case 6: 
        	order.add(Order.asc("pa.nazwisko"));
        	order.add(Order.asc("pa.imie"));
        	break;
        case 7:
        	order.add(Order.desc("pa.nazwisko"));
        	order.add(Order.desc("pa.imie"));
	        break;
        case 8: 
        	order.add(Order.asc("pr.nazwisko"));
        	order.add(Order.asc("pr.imie"));
        	break;
        case 9:
        	order.add(Order.desc("pr.nazwisko"));
        	order.add(Order.desc("pr.imie"));
	        break;
        }
        
        repository.setOrderBy(order);
        
        // szukany tekst jeśli jest zdefiniowany
        String searchText = request.getParameter("search-text");
        if(searchText != null) {
        	repository.setSearchText(searchText);
        	superTable.setExtraLink("&search-text=" + searchText);
        } else {
        	searchText = "";
        }
        
        String dateText = request.getParameter("date-text");
        if(dateText == null) 
        	dateText = todayDate();
        
        superTable.setExtraLink(superTable.getExtraLink() + "&date-text=" + dateText);
        
        logger.trace(dateText);

        String[] dateData = dateText.split("\\.");
        
    	if(dateData.length != 3) {
    		dateText = todayDate();
    		dateData = dateText.trim().split("\\.");
    	}
    	
    	logger.trace(Integer.parseInt(dateData[0]) + " " + Integer.parseInt(dateData[1]) + " " + Integer.parseInt(dateData[2]));
    	
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.YEAR, Integer.parseInt(dateData[0]));
    	cal.set(Calendar.MONTH, Integer.parseInt(dateData[1])-1);
    	cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateData[2]));
        
    	repository.setThatDate(cal.getTime());
    	
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
		logger.trace("Data z calendar: " + format1.format(cal.getTime())); 
        
        // TODO definicja terminu
        
        tableModel.setModelData(repository.getTerminy());
        
        // Rendowanie tekstu tabeli	
     	String newTable = superTable.RenderTable();
     	
     	// Moduł message wyświetlający informacje o poprzedniej akcji z podstrony gdzie wykonano operację
		String successMsg = "";
		
		if(request.getParameter("msg") != null && request.getParameter("text") != null) {
			String msgType = request.getParameter("msg"); // oba muszą być zdefiniowane
			String msgText = request.getParameter("text");
			boolean success = true;
			
			if(msgType.compareToIgnoreCase("new")==0) 
				successMsg = "Dodano nowy termin: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("edit")==0) 
				successMsg = "Zmieniono dane terminu: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("remove")==0) 
				successMsg = "Usunięto rodzaj wizyty: <strong>" + msgText + "</strong>";
			else if(msgType.compareToIgnoreCase("fail")==0) { // ta wiadomość będzie na czerwono
				successMsg = "Nie udało się usunąć terminu: <strong>" + msgText + "</strong>";
				success = false;
			}
			else 
				successMsg = "Wykonano operację";
			
			// zielona czy czerwona wiadomość
			if(success) {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgSuccess", new Assoc[]{
						new Assoc("message", successMsg)
				});
			} else {
				successMsg = new Template().setParentServlet(this).useTemplate("form/msgFail", new Assoc[]{
						new Assoc("message", successMsg)
				});
			}
		}
		
		// rendowanie tabeli już z nagłówkiem i zawartością		
		newTable = new Template().setParentServlet(this).useTemplate("terminy/overviewTable", new Assoc[] {
				new Assoc("tableContent",newTable), 
				new Assoc("searchText",searchText), 
				new Assoc("dateText",dateText), 
				new Assoc("selectSortBox", superTable.GenerateSelectSortBox()),
				new Assoc("sortTargetLink", superTable.getSortTargetLink()),
				new Assoc("recordTargetLink", superTable.getRecordTargetLink()),
				new Assoc("orderLink", superTable.getSortName()),
				new Assoc("message", successMsg),
		});
	
		// finałowe renderowanie strony
		app.FinalRender(newTable, "Terminy", new String[] {"przychodnia", "terminy"});
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);
		response.setHeader("Location", ConfigData.appRootPath + "terminy"); 
	}
	
	private String todayDate() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");	        		
        return format1.format(cal.getTime()); 
	}

	/**
	 * Model danych tabeli
	 */
	public class TerminyTableModel extends AbstractSuperTableModel<TerminEnt> {
	 		    
	    private final static int TERMIN_IDX = 0; // idx dla identyfikacji kolumn pod numerze
	    private final static int WIZYTA_ZABIEG_IDX = 1;
	    private final static int RODZAJ_IDX = 2;
	    private final static int PACJENT_IDX = 3;
	    private final static int LEKARZ_IDX = 4;

	 
	    public TerminyTableModel() {
			columnNames = new String[] {"Termin", "Wiz/Zab", "Rodzaj", "Pacjent", "Lekarz"}; // nazwy w Header
			dataNames = new String[] {"termin", "wizzab", "rodzaj", "pacjent", "lekarz"}; // nazwy dla sort
			// Z tych danych będzie utworzny select box do sortowania !
	    }
	    
	    // wyciąganie danych z listy
	    public String getValueAt(int rowIndex, int columnIndex) {
	 
	        if(data == null) return null;
	        TerminEnt item = data.get(rowIndex);
	        switch (columnIndex) {
            case TERMIN_IDX:
        		Calendar cal = Calendar.getInstance();
        		cal.setTime(item.getTermin());
        		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");	        		
                return format1.format(cal.getTime()); 
            case WIZYTA_ZABIEG_IDX:
            	return item.isZabieg()?"Zabieg":"Wizyta";
            case RODZAJ_IDX:
            	if(item.isZabieg())
            		return item.getRodzajzabiegu().getNazwa();
            	else
            		return item.getRodzajwizyty().getNazwa();
            case PACJENT_IDX:
                return item.getPacjent().getNazwisko() + " " + item.getPacjent().getImie();
            case LEKARZ_IDX:
            	return item.getPracownik().getNazwisko() + " " + item.getPracownik().getImie();
            default:
                return "";
        }
	    }
	    
	    // wyciaganie id rekodru po numerze w liście
		public String getRecordId(int row) {
			return Long.toString(data.get(row).getId());
		}
	    
		// reszta załatwia klasa tabeli
	}
	
}
