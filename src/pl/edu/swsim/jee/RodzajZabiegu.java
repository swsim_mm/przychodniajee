package pl.edu.swsim.jee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.edu.swsim.jee.core.Assoc;
import pl.edu.swsim.jee.core.CoreApp;
import pl.edu.swsim.jee.core.Template;
import pl.edu.swsim.zw.TextTools;
import pl.edu.swsim.zw.dao.GabinetyRep;
import pl.edu.swsim.zw.dao.RodzajeZabiegowRep;
import pl.edu.swsim.zw.entities.GabinetEnt;
import pl.edu.swsim.zw.entities.RodzajZabieguEnt;

/**
 * Servlet implementation class RodzajZabiegu
 */
@WebServlet(
		description = "dane oraz edycja rodzaju zabiegu", 
		urlPatterns = { 
				"/rodzajzabiegu", 
				"/RodzajZabiegu"
		})
public class RodzajZabiegu extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(new Object() { }.getClass().getEnclosingClass());
	
	// Wszystkie pola które mają być przechwycone z postData
		private static String[] formFields = new String[] {
			"id", "nazwa", "rodzaj", "cena", "refundacja", "punkty", "skierowanie", "czas-h", "czas-m", "opis", "gabinet",
		};
		// Strona do której mozna przekierować w razie małych 
		// błedów (danych wpowadzonych przez url)
		public static final String TABLE_PAGE = "rodzajezabiegow"; 
		public static final String FORM_PAGE = "rodzajzabiegu";
	
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RodzajZabiegu() {
        super();
        logger.trace(".");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// gdzie iść przeniesnione w "mode"
		if(request.getParameter("mode") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		}
		
		String formMode = request.getParameter("mode");
		
		/******************************************************
		 * Dla new nie trzeba niczego pobierać na starcie
		 */
		if(formMode.compareToIgnoreCase("new") == 0) {
			NewItem(app, new RodzajZabieguEnt());
			return;
		}
		
		// dla reszty będzie trzeba pobrać id rekordu
		if(request.getParameter("id") == null) {
			app.Redirect(TABLE_PAGE);
			return;
		} 
		
		String strId = request.getParameter("id");
		Long id;
		
		// Próba konwersji tekstu otrzymanego z url na liczbę long
		try {
			id = Long.parseLong(strId);
			if(id < 0)
				throw new Exception("Zły numer id");
		} catch(Exception e) {
			logger.trace(TextTools.twoLine("Błąd parsowania id"));
			app.Redirect(TABLE_PAGE); // taka pomyłka może wyniknać tylko z ingerencji adresem
			
			return;
		}
		
		/******************************************************
		 * Pobranie użytkownika	
		 */	
		ArrayList<RodzajZabieguEnt> data = new RodzajeZabiegowRep().setId(id).getRodzajeZabiegow();
		
		// ostatni raz opusczenie strony gdy coś będzie nie tak
		if(data.size() != 1) {
			logger.trace(TextTools.twoLine("Błąd pobierania rekordu po jego id"));
			app.RedirectError(); 
			return;
		} 
		
		RodzajZabieguEnt item = data.get(0);
		
		// Mając użytkownika można wywołac pożądaną akcję
		if(formMode.compareToIgnoreCase("view") == 0) 
			ViewItem(app, item);
		else if(formMode.compareToIgnoreCase("edit") == 0) 
			EditItem(app, item); 
		else if(formMode.compareToIgnoreCase("remove") == 0) 
			RemoveItem(app, item);
		else 
			app.Redirect(TABLE_PAGE);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CoreApp app = new CoreApp(this, request, response);
		
		// Wyrzucenie niezalogowanych
		if(!app.isLogedIn()) {
			app.RedirectLogin();
			return;
		}
		
		// co będzie się działo zdefinowane w mode
		String formMode = request.getParameter("mode");
		
		// Mając użytkownika można wywołac pożądaną akcję
		// null wymusza pobranie danych z formatek
		if(formMode.compareToIgnoreCase("new") == 0)  {
			NewItem(app, null);
		} else if(formMode.compareToIgnoreCase("view") == 0)  {
			ViewItem(app, null);
		} else if(formMode.compareToIgnoreCase("edit") == 0) {
			EditItem(app, null);
		} else if(formMode.compareToIgnoreCase("remove") == 0) {
			RemoveItem(app, null);
		} else {
			app.Redirect(TABLE_PAGE);
		}	
		
	}
	
	/**
	 * Pobiera dane z formatki i zapisuje je w rekordzie 
	 * @param request
	 * @param validResult
	 * @return
	 */
	private RodzajZabieguEnt getDataResults(HttpServletRequest request, boolean[] validResult) {	
		logger.trace(".");
		boolean isOK = true; // gdy nie wychwycono znaczących błędów
		
		Map<String, String> postData = new HashMap<String, String>();
		
		// pobranie elementów formularza z request
		for(String field: formFields) {
			postData.put(field, request.getParameter(field));		
		}
		
		RodzajZabieguEnt item = new RodzajZabieguEnt();
		
		// id gdy edytowany jest istniejący 
		if(postData.get("id") != null) {
			logger.trace(TextTools.twoLine("ID (string):" + postData.get("id")));
			try { //trochę zachodu z konwersją
				Long id = Long.parseLong(postData.get("id"));
				if(id > 0) {
					item.setId(id); 
					logger.trace(TextTools.twoLine("ID:" + Long.toString(id)));
				}
			} catch (Exception e) { }
		}
		
		
		// TODO - do wywalenia potem + do zrobienia cena i pkt.
		
		
		if(postData.get("czas-h") != null || postData.get("czas-m") != null){
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, (Integer.valueOf(postData.get("czas-h"))));
			cal.set(Calendar.MINUTE, (Integer.valueOf(postData.get("czas-m"))));
			
			item.setCzastrwania(new java.sql.Time(cal.getTimeInMillis()));
		}
		
		

		if(postData.get("nazwa")==null || postData.get("nazwa").length() < 1)
			isOK = false;

		if(postData.get("rodzaj")==null || postData.get("rodzaj").length() < 1)
			isOK = false;
		
		if(postData.get("skierowanie") != null && postData.get("skierowanie").compareToIgnoreCase("true")==0)
			item.setSkierowanie(true);
		
		if(postData.get("refundacja") != null && postData.get("refundacja").compareToIgnoreCase("true")==0)
			item.setRefundacja(true);
	
		//item.setCena(postData.get("cena"));
		
		if(postData.get("punkty")!=null){
			double pkt = Double.parseDouble(postData.get("punkty"));
			float p = Math.round(pkt);
			
			item.setPunkty((int) p);	
		}
		
		if(postData.get("cena")!=null){
			double pkt = Double.parseDouble(postData.get("cena"));
			float p = Math.round(pkt);
			
			item.setCena((int) p);	
		}
		
		//item.setPunkty(postData.get("punkty"));
		
		
		item.setNazwa(postData.get("nazwa"));
		item.setRodzaj(postData.get("rodzaj"));
		item.setOpis(postData.get("opis"));
		
		
		if(postData.get("gabinet") != null && postData.get("gabinet").length() > 0 && postData.get("gabinet").compareToIgnoreCase("0") != 0) {
			try {
				Long prId = Long.parseLong(postData.get("gabinet"));
				
				ArrayList<GabinetEnt> prList = new GabinetyRep().setId(prId).getGabinety();
				if(prList.size() == 1)
					item.setGabinet(prList.get(0));
				
			} catch(NumberFormatException e) {
				isOK = false;
			}
		}
		
						
		validResult[0] = isOK; // java nie przekazuje referencji dla typów prymitywnych, stąd tablica
		return item;
	}
	
	/**
	 * Renderuje pola formatki
	 * @param render
	 * @param item
	 * @return
	 */
	private String RenderForm(Template render, RodzajZabieguEnt item) {
		logger.trace(".");
		
		
		// zbieranie danych do formatki
		ArrayList<Assoc> parm = new ArrayList<Assoc>();
				
		parm.add(new Assoc("id",(Long.toString(item.getId()))));
		parm.add(new Assoc("nazwa",item.getNazwa()));
		parm.add(new Assoc("rodzaj",item.getRodzaj()));
		parm.add(new Assoc("cena",String.valueOf(item.getCena())));
		parm.add(new Assoc("refundacja",(item.isRefundacja()?"checked":"")));
		parm.add(new Assoc("punkty",Integer.toString(item.getPunkty())));
		parm.add(new Assoc("skierowanie",(item.isSkierowanie()?"checked":"")));
		parm.add(new Assoc("opis",item.getOpis()));
		// TODO parm.add(new Assoc("czas",item.getCzastrwania()));
		

		Calendar cal = Calendar.getInstance();
		if(item.getCzastrwania() != null){
			cal.setTime(item.getCzastrwania());
			parm.add(new Assoc("czas-h",(Integer.toString(cal.get(Calendar.HOUR_OF_DAY)))));
			parm.add(new Assoc("czas-m",(Integer.toString(cal.get(Calendar.MINUTE)))));
		}
		
		Long idGabinet = -1L;
		if(item.getGabinet() != null)
			idGabinet = item.getGabinet().getId();
		
		// generowanie select-option z wybraym gabinetem
		String gabinety = "";
		ArrayList<GabinetEnt> gabinetyList = new GabinetyRep().getGabinety();
		for(int i = 0; i<gabinetyList.size(); i++) 
		{
			gabinety += "<option value=\"" + gabinetyList.get(i).getId() + "\"";
			if(gabinetyList.get(i).getId() == idGabinet)
				gabinety += " selected";
			gabinety += ">";
			gabinety += "(" + gabinetyList.get(i).getNumer() + ") " + gabinetyList.get(i).getNazwa();
			gabinety += "</option>";
		}
		
		parm.add(new Assoc("gabinet",gabinety));

		
		Assoc[] assocData = new Assoc[parm.size()];
		assocData = parm.toArray(assocData);

		return render.useTemplate(this, "rodzajezabiegow/rodzajzabieguForm", assocData);
	}
	/**
	 * Akcja nowego elementu
	 * @param app
	 * @param item
	 */
	private void NewItem(CoreApp app, RodzajZabieguEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("New Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		// strona nie jest wyśietlana pierwszy raz, dane zostały przekazane przez formularz
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false; 
		}
		
		Template templ = new Template().setParentServlet(this);
		
		// trzeba wyświetlić formularz
		if(firstRender || !isOK[0]) {
			
			String preFrom = templ.useTemplate("form/formNewBegin", new Assoc[] {
					new Assoc("formTitle","Nowy rodzaj zabiegu"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formNewEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Dodaj rodzaj zabiegu", new String[] {"slowniki", "rodzajezabiegow", "Nowy rodzaj zabiegu"});
			
		} else {
			// nie ma błędów, można wykonać akcję
			new RodzajeZabiegowRep().setRodzajZabiegu(item).AddRodzajZabiegu();
			app.Redirect(TABLE_PAGE + "?msg=new&text=" + item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("New Item end"));

	}
	
	private void EditItem(CoreApp app, RodzajZabieguEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("EditItem"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
			
		Template templ = new Template().setParentServlet(this);
		
		if(firstRender || !isOK[0]) {

			String preFrom = templ.useTemplate("form/formEditBegin", new Assoc[] {
					new Assoc("formTitle","Edytuj rodzaj zabiegu"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
			});
			
			String postForm = templ.useTemplate("form/formEditEnd", new Assoc[] {});
			
			String form = this.RenderForm(templ,item);
						
			app.FinalRender(preFrom + form + postForm, "Edytuj rodzaj zabiegu", new String[] {"slowniki", "rodzajezabiegow", "Edytuj rodzaj zabiegu"});
			
		} else {
						
			new RodzajeZabiegowRep().setRodzajZabiegu(item).EditRodzajZabiegu();
			app.Redirect(TABLE_PAGE + "?msg=edit&text=" + item.getNazwa());
			
		}
		logger.trace(TextTools.oneLine("EditItem end"));
	}
	
	private void ViewItem(CoreApp app, RodzajZabieguEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("ViewItem"));
				
		if(item == null) {
			app.RedirectError();
		}
			
		Template templ = new Template().setParentServlet(this);
		
		String preFrom = templ.useTemplate("form/formViewBegin", new Assoc[] {
				new Assoc("formTitle","Podgląd rodzaju zabiegu"),
				new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
		});
		
		String postForm = templ.useTemplate("form/formViewEnd", new Assoc[] {});
		
		String form = this.RenderForm(templ,item);
					
		app.FinalRender(preFrom + form + postForm, "Podgląd rodzaju zabiegu", new String[] {"slowniki", "rodzajezabiegow", "Podgląd rodzaju zabiegu"});
			
		logger.trace(TextTools.oneLine("ViewItem end"));
		
	}
	
	private void RemoveItem(CoreApp app, RodzajZabieguEnt item) {
		logger.trace(".");
		logger.trace(TextTools.oneLine("Remove Item"));
		
		boolean[] isOK = new boolean[] {true};
		boolean firstRender = true;
		
		if(item == null) {
			item = getDataResults(app.getRequest(), isOK);
			firstRender = false;
		}
		
		Template templ = new Template().setParentServlet(this);
		
		
		if(firstRender) {

			String removeForm = templ.useTemplate("form/formRemove", new Assoc[] {
					new Assoc("formTitle","Usuń rodzaj zabiegu"),
					new Assoc("pageLink",FORM_PAGE),
					new Assoc("id",(item.getId()==0?"":Long.toString(item.getId()))),
					new Assoc("message","Czy napewno chcesz ususnąć rodzaj zabiegu <strong>" + item.getNazwa() + "</strong>?"),
					new Assoc("warning", templ.useTemplate("rodzajezabiegow/removeWarning", new Assoc[]{}))
			});
						
			app.FinalRender(removeForm, "Usuń rodzaj zabiegu", new String[] {"slowniki", "rodzajezabiegow", "Usuń rodzaj zabiegu"});
			
		} else {
			
			logger.trace(item.toString());
			
			Long id = item.getId();
			
			// dociągnięcie danych rodzaju zabiegu
			ArrayList<RodzajZabieguEnt> items = new RodzajeZabiegowRep().setId(id).getRodzajeZabiegow();
 			if(items.size() != 1)
 				app.RedirectError();
 			
 			item = items.get(0); // teraz mamy dane takie jak z id
			
			String msg = item.getNazwa();
			
			new RodzajeZabiegowRep().setRodzajZabiegu(item).DeleteRodzajZabiegu(); // usunięcie 
			
			if(new RodzajeZabiegowRep().setId(id).getRodzajeZabiegow().size() == 0) // sprawdzenie czy się udało usunąć
				app.Redirect(TABLE_PAGE + "?msg=remove&text=" + msg);
			else
				app.Redirect(TABLE_PAGE + "?msg=fail&text=" + msg);
	
		}
		logger.trace(TextTools.oneLine("Remove end"));
	}
	
}
